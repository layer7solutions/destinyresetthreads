"""Functions that build the infomration needed to be inserted into thread templates"""  # pylint: disable=too-many-lines

from collections import OrderedDict
from datetime import datetime, timezone
from logging import Logger
from typing import Dict, List, Union

from bungie_api import BungieAPI
from formatters import (
    format_activity,
    format_armor,
    format_bounty,
    format_item_with_cost,
    format_lost_sectors,
    format_trials_map,
    format_weapon,
    format_weekly_dares_contestants,
    format_weekly_dares_loot,
)
from templates import TEMPLATE_DAILY, TEMPLATE_IB, TEMPLATE_TRIALS, TEMPLATE_WEEKLY, TEMPLATE_XUR

DAILY_BOUNTY_VENDORS: Dict[int, Dict[str, str]] = {
    # Tower
    919809084: {"index": "Dawning Bounties", "name": "Eva Levante, Charming Old Lady"},
    # 69482069: {"index": "Bounties", "name": "Commander Zavala, Vanguard"},
    # 3603221665: {"index": "Bounties", "name": "Lord Shaxx, Crucible"},
    765357505: {"index": "Bounties", "name": "Saint-14, Trials of Osiris"},
    # 248695599: {"index": "Bounties", "name": "The Drifter, Gambit"},
    672118013: {"index": "Bounties", "name": "Banshee-44, Gunsmith"},
    # Red War and its remnants
    # 396892126: {"index": "Bounties", "name": "Devrim Kay, European Dead Zone"},
    # 1576276905: {"index": "Bounties", "name": "Failsafe, Nessus"},
    # Forsaken
    # 1841717884: {"index": "Available Bounties", "name": "Petra Venj, The Dreaming City"},
    # Shadowkeep
    # 1616085565: {"index": "Moon Bounties", "name": "Eris Morn, Moon"},
    # 3411552308: {"index": "Nightmare Bounties", "name": "Lectern of Enchantment, Moon"},
    # Beyond Light
    # 2531198101: {"index": "Europa Bounties", "name": "Variks, Europa"},
    # 1816541247: {"index": "Bounties", "name": "Shaw Han, Cosmodrome"},
    # Bungie's 30th Anniversary
    # 3442679730: {"index": "Bounties", "name": "Xûr, Eternity"},
    # 3431983428: {"index": "Starhorse Bounties", "name": "Starhorse, Eternity"},
    # The Witch Queen
    # 2384113223: {"index": "Bounties", "name": "Fynch, Savathûn's Throne World"},
    # Lightfall
    # 1021220385: {"index": "Bounties", "name": "Nimbus, Neomuna"},
    # SEASON TEMP
    # SEASON TEMP END
}
WEEKLY_BOUNTY_VENDORS: Dict[int, Dict[str, str]] = {
    # Tower
    919809084: {"index": "Dawning Bounties", "name": "Eva Levante, Charming Old Lady"},
    # Forsaken
    1841717884: {"index": "Available Bounties", "name": "Petra Venj, The Dreaming City"},
    # Shadowkeep
    1616085565: {"index": "Moon Bounties", "name": "Eris Morn, Moon"},
    3411552308: {"index": "Nightmare Bounties", "name": "Lectern of Enchantment, Moon"},
    # Beyond Light
    2531198101: {"index": "Europa Bounties", "name": "Variks, Europa"},
    1816541247: {"index": "Bounties", "name": "Shaw Han, Cosmodrome"},
    # Bungie's 30th Anniversary
    3431983428: {"index": "Starhorse Bounties", "name": "Starhorse, Eternity"},
    # Lightfall
    1021220385: {"index": "Bounties", "name": "Nimbus, Neomuna"},
}
WEEKLY_ARMOR_VENDORS: Dict[int, Dict[str, str]] = {
    69482069: {"index": "Featured Armor", "name": "Vanguard"},
    3603221665: {"index": "Featured Armor", "name": "Crucible"},
    248695599: {"index": "Featured Armor", "name": "Gambit"},
    396892126: {"index": "Faction Rewards", "name": "EDZ"},
    1576276905: {"index": "Faction Rewards", "name": "Nessus"},
    350061650: {"index": "Armor", "name": "Ada-1"},
}

# The start date is just an arbitary date the rotation starts, don't think too much about it.
DREAMING_CITY_ROTATION_START: datetime = datetime(2018, 10, 2, hour=17, tzinfo=timezone.utc)
DREAMING_CITY_CURSE_ROTATION: List[str] = ["Weak", "Growing", "Strong"]
DREAMING_CITY_PETRA_ROTATION: List[str] = ["The Strand", "Divalian Mists", "Rheasilvia"]
DREAMING_CITY_MISSION_ROTATION: List[int] = [1640956655, 3746811765, 1313738982]
DREAMING_CITY_ASCENDANT_ROTATION: List[str] = [
    "Agonarch Abyss, Bay of Drowned Wishes",
    "Cimmerian Garrison, Chamber of Starlight",
    "Ouroborea, Aphelion's Rest",
    "Forfeit Shrine, Gardens of Esila",
    "Shattered Ruins, Spine of Keres",
    "Keep of Honed Edges, Harbinger's Seclude",
]
DREAMING_CITY_WELL_ROTATION = [
    "Scorn, Plagues: Sikariis & Varkuuriis",
    "Hive, Plague: Cragur",
    "Taken, Plague: Inomina",
]

# just like above, the start date is without further meaning
SHADOWKEEP_START_DATE: datetime = datetime(2019, 10, 1, hour=17, tzinfo=timezone.utc)
NIGHTMARE_ROTATION_START: datetime = datetime(2019, 11, 26, hour=17, tzinfo=timezone.utc)
NIGHTMARE_ROTATION: List[str] = [
    "Nightmare of Xortal, Sworn of Crota (Sorrow's Harbor)",
    "Nightmare of Horkis, Fear of Mithrax (Anchor of Light)",
    "Nightmare of Jaxx, Claw of Xivu Arath (Hellmouth)",
    "Fallen Council (Archer's Line)",
]
ALTAR_WEAPON_ROTATION: List[int] = [2164448701, 3067821200, 2782847179]  # Apostate  # Heretic  # Blasphemer
TROVE_GUARDIAN_ROTATION: List[str] = [
    "Archer's Line",
    "Anchor of Light",
    "Hellmouth",
]
SHADOWKEEP_CAMPAIGN_MISSION_ROTATION: List[int] = [
    2306231495,  # A Mysterious Disturbance
    471727774,  # In The Deep
    1326496189,  # Beyond
]

EUROPA_ROTATION_START: datetime = datetime(2020, 11, 10, hour=17, tzinfo=timezone.utc)
EUROPA_ECLIPSED_ZONE_ROTATION: List[str] = ["Cadmus Ridge", "Eventide Ruins", "Asterion Abyss"]
NIGHTFALL_ROTATION_START: datetime = datetime(2021, 5, 11, hour=17, tzinfo=timezone.utc)

# Bungie 30th Anniversary
# Launched December 07, 2021 at weekly reset
BUNGIE_30TH_ANNIVERSARY_START_DATE: datetime = datetime(2021, 12, 7, hour=17, tzinfo=timezone.utc)
# Dares loot rotation. Updated as of Season of the Seraph (Season 19)
# Weapons are a list of item hashes, armor is just a string since looking all of those would be a pain
DARES_ROTATION_START: datetime = datetime(2022, 12, 6, hour=17, tzinfo=timezone.utc)
DARES_LOOT_ROTATION: List[Dict[str, List[Union[int, str]]]] = [
    # Example format: {"weapons": [hash, ...], "armor": [str, ...]}
    # Week 1
    {
        "weapons": [
            720351795,
            2453357042,
            2714022207,
            981718087,
            2742838701,
            1723380073,
            1786797708,
            1162247618,
            2776503072,
            2742838700,
            1513993763,
        ],
        "armor": ["Scatterhorn Suit", "Wild Hunt Suit"],
    },
    # Week 2
    {
        "weapons": [
            602618796,
            1097616550,
            2121785039,
            3075224551,
            2496242052,
            3460122497,
            893527433,
            2857348871,
            2807687156,
            2257180473,
            3743729616,
            6857689,
        ],
        "armor": ["Scatterhorn Suit", "Praefectur Suit"],
    },
    # Week 3
    {
        "weapons": [
            1119734784,
            304659313,
            1621558458,
            541188001,
            599895591,
            2434225986,
            253196586,
            3745990145,
            2009277538,
            2957367743,
            3356526253,
            188882152,
        ],
        "armor": ["Scatterhorn Suit", "Lightkin Suit"],
    },
    # Week 4
    {
        "weapons": [
            3184681056,
            1337707096,
            1622998472,
            3472875143,
            3044460004,
            2066434718,
            577528837,
            3409645497,
            4146702548,
            4230993599,
            2588048270,
        ],
        "armor": ["Scatterhorn Suit", "Pathfinder Suit"],
    },
]

# The Witch Queen
# The date The Witch Queen released - 2022-02-22. Used as the basis for all expansion-related rotations.
WITCH_QUEEN_START_DATE: datetime = datetime(2022, 2, 22, hour=17, tzinfo=timezone.utc)
# Wellspring has a four-day rotation
WELLSPRING_WEAPON_ROTATION: List[int] = [
    # https://www.reddit.com/r/DestinyTheGame/comments/t1ux3g/the_wellspringattackdefend_drop_rotation/
    927567426,  # Day 1: Attack: Come to Pass Auto Rifle
    2721157927,  # Day 2: Defend: Tarnation Grenade Launcher
    1399109800,  # Day 3: Attack: Fel Taradiddle Combat Bow
    3865728990,  # Day 4: Defend: Father's Sins Sniper Rifle
]
# The campaign missions are on a weekly rotation
WITCH_QUEEN_CAMPAIGN_MISSION_ROTATION: List[int] = [
    658166179,  # The Arrival
    1462428868,  # The Ghosts
    2463352493,  # The Communion
    4056346870,  # The Cunning
    1591784633,  # The Last Chance
    153838906,  # The Ritual
]

# Lightfall
# The date Lightfall released - 2023-02-28. Used as the basis for all expansion-related rotations.
LIGHTFALL_START_DATE: datetime = datetime(2023, 2, 28, hour=17, tzinfo=timezone.utc)
# Terminal Overload has a 3-day rotation
TERMINAL_OVERLOAD_LOCATION_ROTATION: List[str] = [
    "Zephyr Concourse",
    "Ahimsa Park",
    "Límíng Harbor",
]
TERMINAL_OVERLOAD_WEAPON_ROTATION: List[int] = [
    2187717691,  # Circular Logic Machine Gun
    2573900604,  # Basso Ostinato Shotgun
    811403305,  # Synchronic Roulette Submachine Gun
]
NEOMUNA_INCURSION_ZONE_ROTATION: List[str] = ["Límíng Harbor", "Zephyr Concourse", "Ahimsa Park"]
NEOMUNA_PARTITION_ROTATION: List[int] = [
    708242452,  # Hard Reset
    1026439577,  # Backdoor
    227379247,  # Ordnance
]
LIGHTFALL_CAMPAIGN_MISSION_ROTATION: List[int] = [
    1688654730,  # First Contact
    2314609324,  # Downfall
    4024253728,  # Breakneck
    1266868740,  # No Time Left
    1152906386,  # Desperate Measures
]

# The Final Shape
# across weeks - Techman
FINAL_SHAPE_START_DATE: datetime = datetime(2024, 6, 4, hour=17, tzinfo=timezone.utc)
PALE_HEART_COOP_FOCUS_MISSION_ROTATION: List[int] = [
    230477216,  # Ascent: Cooperative Focus Campaign
    1329796464,  # Dissent: Cooperative Focus Campaign
    1475344277,  # Iconoclasm: Cooperative Focus Campaign
]
# Overthrow is a daily rotation
PALE_HEART_OVERTHROW_ROTATION: List[str] = ["The Impasse", "The Blooming", "The Landing"]
PALE_HEART_OVERTHROW_ROTATION_DESC: List[str] = [
    "An endless tide of the Witness's forces are converging on the Impasse.",  # The Impasse
    "The forces of the Witness and the Lucent Hive battle for control of the Blooming.",  # The Blooming
    "The Lucent Hive are laying siege to the Landing to try and wrest control of the Traveler's Light.",  # The Landing
]

# DestinyInventoryItemDefinition hashes
NIGHTFALL_LOOT_ROTATION = []

# Activity hashes for raids, used in the weekly reset threads to get active challenges
RAIDS = [
    2122313384,  # Last Wish: Level 55
    1042180643,  # Garden of Salvation
    910380154,  # Deep Stone Crypt
    3881495763,  # Vault of Glass: Normal
    # 1681562271,  # Vault of Glass: Master
    1441982566,  # Vow of the Disciple: Normal
    # 4217492330,  # Vow of the Disciple: Master
    1374392663,  # King's Fall: Normal
    # 2964135793,  # King's Fall: Master
    2381413764,  # Root of Nightmares: Normal
    # 2918919505,  # Root of Nightmares: Master
    4179289725,  # Crota's End: Normal
    # 1507509200,  # Crota's End: Master
    1541433876,  # Salvation's Edge
    # 4129614942,  # Salvation's Edge: Master
]

# SEASON TEMP
# The season start date is used to calculate the lost sector rotation, etc.
SEASON_START_DATE: datetime = datetime(2025, 2, 4, hour=17, tzinfo=timezone.utc)
# Seasonal PresentationNodes for seasonal challenges: week 1, 2, 3, ..., 10
# TODO: Check in Frontiers if these PresentationNodes remain the same
# weekly: 3109663559 (week 1, week 2, ...)
# seasonal: 1441306274 (contains the "complete challenges for big bright dust" challenge)
SEASON_RECORD_ROTATION: List[int] = [
    3288252652,
    3288252655,
    3288252654,
    3288252649,
    3288252648,
    3288252651,
    3288252650,
    3288252645,
    3288252644,
    1980347316,
    1980347317,
    1980347318,
    1980347319,
    1980347312,
    1980347313,
    1980347314,
]
# What gear is the lost sector dropping, if solo?
# This rotation can change from season to season, but it seems to be consistent starting from S16
# Deprecated as of Final Shape: now only drops an exotic engram, but keeping this here in case that ever changes
# As of Final Shape, it has been re-added to Rahool as a free focusing option
SEASON_LOST_SECTOR_ITEM_ROTATION: List[str] = ["Chest", "Helmet", "Legs", "Arms"]
# Seasonal activities
SEASON_ACTIVITIES: List[int] = []
# SEASON TEMP END


def get_daily(api: BungieAPI, logger: Logger) -> str:
    """Get information for daily threads"""
    # default values if something fails
    PLACEHOLDER = "*beep boop* failed to fetch"  # pylint: disable=invalid-name
    dailyms = PLACEHOLDER
    rahool_materials = PLACEHOLDER
    wellspring_mode = PLACEHOLDER
    terminal_location = PLACEHOLDER

    EMPTY_PLACEHOLDER = ""  # pylint: disable=invalid-name
    gunsmith = EMPTY_PLACEHOLDER
    mods = EMPTY_PLACEHOLDER
    vanguard_ops = EMPTY_PLACEHOLDER
    dares = EMPTY_PLACEHOLDER
    onslaught = EMPTY_PLACEHOLDER
    bounties = EMPTY_PLACEHOLDER
    altar_weapon = EMPTY_PLACEHOLDER
    wellspring_weapon = EMPTY_PLACEHOLDER
    terminal_weapon = EMPTY_PLACEHOLDER
    overthrow = EMPTY_PLACEHOLDER
    overthrow_desc = EMPTY_PLACEHOLDER
    # SEASON TEMP
    activitymodifs = EMPTY_PLACEHOLDER
    # These are the default values if SEASON_LOST_SECTOR_* is empty
    lost_sector = "*Lost Sector information is currently unavailable*. Check the comments!"
    # SEASON TEMP END

    try:
        # STATIC STUFF - ROTATIONS ONLY
        # moon nightmare stuff
        logger.info("daily: getting static altar stuff")
        altar_days = (datetime.now(timezone.utc) - NIGHTMARE_ROTATION_START).days
        altar_weapon_hash = ALTAR_WEAPON_ROTATION[altar_days % len(ALTAR_WEAPON_ROTATION)]
        altar_item = api.item_def(altar_weapon_hash)
        altar_weapon = "[{}](https://light.gg/db/items/{}) ({})\n".format(
            altar_item["displayProperties"]["name"], altar_weapon_hash, altar_item["itemTypeDisplayName"]
        )

        # Witch Queen - Wellspring
        logger.info("daily: getting static wellspring stuff")
        wellspring_days = (datetime.now(timezone.utc) - WITCH_QUEEN_START_DATE).days
        wellspring_weapon_hash = WELLSPRING_WEAPON_ROTATION[wellspring_days % len(WELLSPRING_WEAPON_ROTATION)]
        if (wellspring_days % len(WELLSPRING_WEAPON_ROTATION)) % 2 == 0:
            wellspring_mode = "Attack"
        else:
            wellspring_mode = "Defend"
        wellspring_item = api.item_def(wellspring_weapon_hash)
        wellspring_weapon = "[{}](https://light.gg/db/items/{}) ({})".format(
            wellspring_item["displayProperties"]["name"], wellspring_weapon_hash, wellspring_item["itemTypeDisplayName"]
        )

        # Lightfall
        logger.info("daily: getting static lightfall stuff")
        terminal_days = (datetime.now(timezone.utc) - LIGHTFALL_START_DATE).days
        terminal_weapon_hash = TERMINAL_OVERLOAD_WEAPON_ROTATION[terminal_days % len(TERMINAL_OVERLOAD_WEAPON_ROTATION)]
        terminal_item = api.item_def(terminal_weapon_hash)
        terminal_location = TERMINAL_OVERLOAD_LOCATION_ROTATION[
            terminal_days % len(TERMINAL_OVERLOAD_LOCATION_ROTATION)
        ]
        terminal_weapon = "[{}](https://light.gg/db/items/{}) ({})".format(
            terminal_item["displayProperties"]["name"], terminal_weapon_hash, terminal_item["itemTypeDisplayName"]
        )

        # Final Shape
        logger.info("daily: getting static final shape stuff")
        overthrow_days = (datetime.now(timezone.utc) - FINAL_SHAPE_START_DATE).days
        overthrow = PALE_HEART_OVERTHROW_ROTATION[overthrow_days % len(PALE_HEART_OVERTHROW_ROTATION)]
        overthrow_desc = PALE_HEART_OVERTHROW_ROTATION_DESC[overthrow_days % len(PALE_HEART_OVERTHROW_ROTATION_DESC)]

        # DYNAMIC STUFF - NEEDS CHARACTER API CALLS
        # LS stuff
        # Lost Sectors are activity interactables (totems/flags) located in the game world, not launchable
        # via director. This means it will not show up in the activity list.
        logger.info("daily: getting static lost sectors")
        activities_from_interactables = api.get_activities_from_interactables()
        expert_hash = 0
        master_hash = 0
        for activity_hash in activities_from_interactables:
            activity_def = api.activity_def(activity_hash)
            if "directActivityModeHash" not in activity_def:
                continue
            if activity_def["directActivityModeHash"] == 103143560:  # <ActivityMode "Lost Sector">
                if "Expert" in activity_def["displayProperties"]["name"]:
                    expert_hash = activity_def["hash"]
                else:
                    master_hash = activity_def["hash"]
        lost_sector = format_lost_sectors(api, [expert_hash, master_hash])

        activities = api.get_activities_full()

        # daily strike modifiers
        logger.info("daily: getting strike modifiers")
        vanguard_ops_activity_hash = 743628305  # activity hash
        if vanguard_ops_activity_hash in activities:
            vanguard_ops_activity = activities[vanguard_ops_activity_hash]
            vanguard_ops = format_activity(
                api=api,
                activity_hash=vanguard_ops_activity_hash,
                h=2,
                description=False,
                link=False,
                modifiers=vanguard_ops_activity["modifierHashes"],
                exclude=None,
            )

        logger.info("daily: getting dares modifiers")
        dares_activity_hash = 265600452
        if dares_activity_hash in activities:
            dares_activity = activities[dares_activity_hash]
            dares = format_activity(
                api=api,
                activity_hash=dares_activity_hash,
                h=2,
                original=True,
                description=False,
                link=False,
                modifiers=dares_activity["modifierHashes"],
                exclude=None,
            )

        logger.info("daily: getting onslaught modifiers")
        onslaught_playlist_activity_hash = 647593269
        if onslaught_playlist_activity_hash in activities:
            onslaught_playlist_activity = activities[onslaught_playlist_activity_hash]
            onslaught = format_activity(
                api=api,
                activity_hash=onslaught_playlist_activity_hash,
                h=2,
                original=True,
                description=False,
                link=False,
                modifiers=onslaught_playlist_activity["modifierHashes"],
                exclude=["rules"],
            )

        # gunsmith & ada
        logger.info("daily: getting gunsmith")
        banshee = api.get_vendor(672118013)
        for catname, catsales in banshee.items():
            if catname == "Featured":
                for inventory_item in catsales:
                    gunsmith += format_weapon(inventory_item, api, True)

        # rahool
        logger.info("daily: getting rahool material exchange")
        # As of Season 21, Rahool's Material Exchange is a sub-vendor
        # Rahool: 2255782930
        # Material Exchange: 2199358137
        rahool = api.get_vendor(2199358137)
        rahool_materials = ""
        for category, inventory in rahool.items():
            if category == "Curios" or category == "Material Exchange":
                for inventory_item in inventory:
                    item = api.item_def(inventory_item["itemHash"])
                    costs = ""
                    for cost in inventory_item["costs"]:
                        cost_item = api.item_def(cost["itemHash"])
                        if costs != "":
                            costs += " & "
                        costs += "{} {}".format(cost["quantity"], cost_item["displayProperties"]["name"])
                    rahool_materials += "* {} ({} for {})\n".format(
                        item["displayProperties"]["name"], inventory_item["quantity"], costs
                    )

        # bounties
        logger.info("daily: getting bounties")
        for vendor, data in DAILY_BOUNTY_VENDORS.items():
            try:
                if vendor == 919809084 or vendor == 9198090841:  # EVA SPECIAL WHOOP WHOOP
                    try:
                        wares = eva_wares
                    except Exception:  # pylint: disable=broad-except
                        all_wares = {}
                        all_wares["T"] = api.get_vendor(919809084, "Titan")
                        all_wares["H"] = api.get_vendor(919809084, "Hunter")
                        all_wares["W"] = api.get_vendor(919809084, "Warlock")

                        wares_handled = []
                        wares = OrderedDict()
                        for _, ware_cats in all_wares.items():
                            for cat, items in ware_cats.items():
                                if cat not in wares:
                                    wares[cat] = []
                                for item in items:
                                    if item["itemHash"] in wares_handled:
                                        continue
                                    wares[cat].append(item)
                                    wares_handled.append(item["itemHash"])
                        eva_wares = wares
                else:
                    wares = api.get_vendor(vendor)
            except Exception:  # pylint: disable=broad-except
                if vendor == 765357505 or vendor == 919809084:  # No exception needed for Saint & Eva
                    logger.info("failed to get vendor %s, hash %i", data["name"], vendor)
                else:
                    logger.exception("failed to get vendor %s, hash %i", data["name"], vendor)
                continue

            if data["index"] not in wares:
                continue
            bounties += "**{}**\n\nName|Description|Requirement|Reward\n-|-|-|-|-\n".format(data["name"])
            for bounty in wares[data["index"]]:
                bounties += format_bounty(bounty, api)
            bounties += "\n\n"

        # SEASON TEMP
        # daily activity modifiers
        logger.info("daily: getting activity modifiers")
        for seasonal_activity_id in SEASON_ACTIVITIES:
            if seasonal_activity_id in activities:
                activity = activities[seasonal_activity_id]
                activitymodifs += (
                    format_activity(
                        api=api,
                        activity_hash=seasonal_activity_id,
                        h=2,
                        original=True,
                        description=False,
                        link=False,
                        modifiers=activity["modifierHashes"],
                        exclude=None,
                    )
                    + "\n"
                )
        # SEASON TEMP END

    except Exception:  # pylint: disable=broad-except
        logger.exception("Error creating Daily Thread")

    return api.process_string_variables(
        TEMPLATE_DAILY.format(
            dailyms=dailyms,
            gunsmith=gunsmith,
            vanguardops=vanguard_ops,
            dares=dares,
            onslaught=onslaught,
            bounties=bounties,
            rahoolmats=rahool_materials,
            altarweapon=altar_weapon,
            wellspringmode=wellspring_mode,
            wellspringweapon=wellspring_weapon,
            terminallocation=terminal_location,
            terminalweapon=terminal_weapon,
            overthrow=overthrow,
            overthrowdesc=overthrow_desc,
            mods=mods,
            # SEASON TEMP
            activitymodifs=activitymodifs,
            ls=lost_sector,
            # SEASON TEMP END
        )
    )


def get_weekly(api: BungieAPI, logger: Logger) -> str:
    """Get information for weekly thread"""
    # default values if something fails
    PLACEHOLDER = "*beep boop* failed to fetch"  # pylint: disable=invalid-name

    # We define and set each variable on a separate line to stop Black from garbling this into a technically correct
    # but unreadable mess
    activity = PLACEHOLDER
    heroicstrikeburn = PLACEHOLDER
    nightfall = PLACEHOLDER
    shadowkeep_campaign_mission = PLACEHOLDER
    wanderingnm = PLACEHOLDER
    ev_bd = PLACEHOLDER
    empire_hunt = PLACEHOLDER
    exo_challenge = PLACEHOLDER
    weekly_dares_contestants = PLACEHOLDER
    witchqueen_campaign_mission = PLACEHOLDER
    lightfall_campaign_mission = PLACEHOLDER
    neomuna_parition_name = PLACEHOLDER
    neomuna_incursion_zone = PLACEHOLDER
    raids = PLACEHOLDER

    # Default blank values - do not change these unless you also check the code that uses them
    EMPTY_PLACEHOLDER = ""  # pylint: disable=invalid-name
    bounties = EMPTY_PLACEHOLDER
    weekly_raid = EMPTY_PLACEHOLDER
    weekly_dungeon = EMPTY_PLACEHOLDER
    weekly_exotic_mission = EMPTY_PLACEHOLDER
    cityweek = EMPTY_PLACEHOLDER
    citypetra = EMPTY_PLACEHOLDER
    citymission = EMPTY_PLACEHOLDER
    citychallenge = EMPTY_PLACEHOLDER
    citywell = EMPTY_PLACEHOLDER
    nfmods = EMPTY_PLACEHOLDER
    rotationgnms = EMPTY_PLACEHOLDER
    troveguardian = EMPTY_PLACEHOLDER
    nfreward = EMPTY_PLACEHOLDER
    eclipsed_zone = EMPTY_PLACEHOLDER
    armor_sales = EMPTY_PLACEHOLDER
    weekly_records = EMPTY_PLACEHOLDER
    witchqueen_campaign_desc = EMPTY_PLACEHOLDER
    lightfall_campaign_desc = EMPTY_PLACEHOLDER
    neomuna_partition_desc = EMPTY_PLACEHOLDER
    final_shape_campaign_mission = EMPTY_PLACEHOLDER
    final_shape_campaign_desc = EMPTY_PLACEHOLDER
    weekly_dares_loot = EMPTY_PLACEHOLDER
    ada = EMPTY_PLACEHOLDER
    # SEASON TEMP
    # SEASON TEMP END

    total_limit = 59
    single_limit = 26

    try:
        # STATIC STUFF - ROTATIONS ONLY
        # Dreaming City
        logger.info("weekly: getting Dreaming City stuff")
        dc_days = (datetime.now(timezone.utc) - DREAMING_CITY_ROTATION_START).days
        dc_weeks = dc_days // 7
        cityweek = DREAMING_CITY_CURSE_ROTATION[dc_weeks % len(DREAMING_CITY_CURSE_ROTATION)]
        citypetra = DREAMING_CITY_PETRA_ROTATION[dc_weeks % len(DREAMING_CITY_PETRA_ROTATION)]
        dc_mission_def = api.activity_def(
            DREAMING_CITY_MISSION_ROTATION[dc_weeks % len(DREAMING_CITY_MISSION_ROTATION)]
        )
        citymission = "*{}* - {}".format(
            dc_mission_def["displayProperties"]["name"], dc_mission_def["displayProperties"]["description"]
        )
        citychallenge = DREAMING_CITY_ASCENDANT_ROTATION[dc_weeks % len(DREAMING_CITY_ASCENDANT_ROTATION)]
        citywell = DREAMING_CITY_WELL_ROTATION[dc_weeks % len(DREAMING_CITY_WELL_ROTATION)]

        # moon stuff
        logger.info("weekly: getting static moon stuff")
        moon_days = (datetime.now(timezone.utc) - NIGHTMARE_ROTATION_START).days
        moon_weeks = moon_days // 7
        wanderingnm = NIGHTMARE_ROTATION[moon_weeks % len(NIGHTMARE_ROTATION)]
        troveguardian = TROVE_GUARDIAN_ROTATION[moon_weeks % len(TROVE_GUARDIAN_ROTATION)]

        moon_c_days = (datetime.now(timezone.utc) - SHADOWKEEP_START_DATE).days
        moon_c_weeks = moon_c_days // 7
        moon_c_hash = SHADOWKEEP_CAMPAIGN_MISSION_ROTATION[moon_c_weeks % len(SHADOWKEEP_CAMPAIGN_MISSION_ROTATION)]
        moon_c_def = api.activity_def(moon_c_hash)
        shadowkeep_campaign_mission = moon_c_def["displayProperties"]["name"]

        # europa stuff
        logger.info("weekly: getting static europa stuff")
        europa_days = (datetime.now(timezone.utc) - EUROPA_ROTATION_START).days
        europa_weeks = europa_days // 7
        eclipsed_zone = EUROPA_ECLIPSED_ZONE_ROTATION[europa_weeks % len(EUROPA_ECLIPSED_ZONE_ROTATION)]

        # dares stuff
        logger.info("weekly: getting static dares stuff")
        dares_weeks = (datetime.now(timezone.utc) - DARES_ROTATION_START).days // 7
        dares_loot_table = DARES_LOOT_ROTATION[dares_weeks % len(DARES_LOOT_ROTATION)]
        weekly_dares_loot = format_weekly_dares_loot(api=api, loot=dares_loot_table)

        # witch queen stuff
        logger.info("weekly: getting static witch queen stuff")
        witchqueen_days = (datetime.now(timezone.utc) - WITCH_QUEEN_START_DATE).days
        witchqueen_weeks = witchqueen_days // 7
        witchqueen_campaign_mission_hash = WITCH_QUEEN_CAMPAIGN_MISSION_ROTATION[
            witchqueen_weeks % len(WITCH_QUEEN_CAMPAIGN_MISSION_ROTATION)
        ]
        witchqueen_campaign_mission_def = api.activity_def(witchqueen_campaign_mission_hash)

        witchqueen_campaign_mission = witchqueen_campaign_mission_def["originalDisplayProperties"]["name"]
        witchqueen_campaign_desc = witchqueen_campaign_mission_def["originalDisplayProperties"]["description"]

        # lightfall stuff
        logger.info("weekly: getting static lightfall stuff")
        lightfall_days = (datetime.now(timezone.utc) - LIGHTFALL_START_DATE).days
        lightfall_weeks = lightfall_days // 7
        lightfall_campaign_mission_hash = LIGHTFALL_CAMPAIGN_MISSION_ROTATION[
            lightfall_weeks % len(LIGHTFALL_CAMPAIGN_MISSION_ROTATION)
        ]
        lightfall_campaign_mission_def = api.activity_def(lightfall_campaign_mission_hash)
        lightfall_campaign_mission = lightfall_campaign_mission_def["originalDisplayProperties"]["name"]
        lightfall_campaign_desc = lightfall_campaign_mission_def["originalDisplayProperties"]["description"]
        neomuna_incursion_zone = NEOMUNA_INCURSION_ZONE_ROTATION[lightfall_weeks % len(NEOMUNA_INCURSION_ZONE_ROTATION)]
        neomuna_partition_hash = NEOMUNA_PARTITION_ROTATION[lightfall_weeks % len(NEOMUNA_PARTITION_ROTATION)]
        neomuna_partition_def = api.activity_def(neomuna_partition_hash)
        neomuna_parition_name = neomuna_partition_def["displayProperties"]["name"]
        neomuna_partition_desc = neomuna_partition_def["displayProperties"]["description"]

        # final shape stuff
        logger.info("weekly: getting static final shape stuff")
        final_shape_days = (datetime.now(timezone.utc) - FINAL_SHAPE_START_DATE).days
        final_shape_weeks = final_shape_days // 7
        final_shape_coop_mission_hash = PALE_HEART_COOP_FOCUS_MISSION_ROTATION[
            final_shape_weeks % len(PALE_HEART_COOP_FOCUS_MISSION_ROTATION)
        ]
        final_shape_coop_mission_def = api.activity_def(final_shape_coop_mission_hash)
        final_shape_campaign_mission = final_shape_coop_mission_def["originalDisplayProperties"]["name"]
        final_shape_campaign_desc = final_shape_coop_mission_def["originalDisplayProperties"]["description"]

        # Seasonal Challenge stuff
        logger.info("weekly: getting seasonal challenges")
        season_days = (datetime.now(timezone.utc) - SEASON_START_DATE).days
        season_weeks = season_days // 7
        # If we have covered fewer weeks than the number of challenge weeks, we have challenges
        if season_weeks < len(SEASON_RECORD_ROTATION) and len(SEASON_RECORD_ROTATION) > 0:
            current_record = SEASON_RECORD_ROTATION[season_weeks % len(SEASON_RECORD_ROTATION)]
            node = api.presentation_node_def(current_record)
            if node is not None:
                for record_id in node["children"]["records"]:
                    record = api.record_def(record_id["recordHash"])
                    # Workaround for when a seasonal challenge is classified
                    if record["displayProperties"]["name"] == "Classified":
                        weekly_records += "{}|{}| | \n".format(
                            record["displayProperties"]["name"], record["displayProperties"]["description"]
                        )
                        continue
                    objectives = ""
                    for obj_hash in record["objectiveHashes"]:
                        obj = api.objective_def(obj_hash)
                        if objectives != "":
                            objectives += " & "
                        objectives += "{} {}".format(obj["completionValue"], obj["progressDescription"])
                    rewards = ""
                    for reward in record["rewardItems"]:
                        if reward["itemHash"] == 0:
                            continue
                        # Nicer Display of "zero"/"one" rewards
                        if reward["quantity"] == "":
                            reward["quantity"] = 0
                        if int(reward["quantity"]) < 2:
                            reward["quantity"] = ""
                        reward_info = api.item_def(reward["itemHash"])
                        if rewards != "":
                            rewards += " & "
                        rewards += "{} {}".format(reward["quantity"], reward_info["displayProperties"]["name"])
                    weekly_records += "{}|{}|{}|{}\n".format(
                        record["displayProperties"]["name"],
                        record["displayProperties"]["description"].replace("\r", "").replace("\n", " "),
                        objectives,
                        rewards,
                    )
            else:
                logger.warning(
                    "PresentationNode %s returned None in manifest! https://data.destinysets.com/i/PresentationNode:%s",
                    current_record,
                    current_record,
                )
                weekly_records = "*beep boop* failed to fetch"
        else:
            weekly_records = "*No more records this season!*"

        # DYNAMIC STUFF - NEEDS CHARACTER API CALLS
        # nightfall
        logger.info("weekly: getting nightfall")
        # 2443315975 -> Weekly Completion Challenge
        activities_nf = api.get_activities_with_objective(1612424695)
        oldmods = []
        for activity in activities_nf:
            if "modifierHashes" not in activity:
                continue
            nf_info = api.activity_def(activity["activityHash"])
            nf_hash = activity["activityHash"]
            if not nf_info:
                continue
            # As of Season of the Deep (S21), the Grandmaster node also contains the normal Nightfall objective. Filter
            # those nodes out, only keeping Nightfall: Hero, Nightfall: Legend, etc.
            if nf_info["displayProperties"]["name"][:10] != "Nightfall:":
                continue
            nfmods += "* {}\n".format(nf_info["displayProperties"]["name"])
            if nightfall == "*beep boop* failed to fetch":
                nightfall = "[{}](https://destinytracker.com/destiny-2/db/activities/{})".format(
                    nf_info["displayProperties"]["description"], nf_hash
                )
            else:
                nfmods += "  * *All previous modifiers*\n"
            for mod in activity["modifierHashes"]:
                # If the modifer has already been processed, skip
                if mod in oldmods:
                    continue
                mod_info = api.activity_modifier_def(mod)
                mod_name = mod_info["displayProperties"]["name"]
                mod_description = (
                    mod_info["displayProperties"]["description"]
                    .replace("\r\n", "\n")
                    .replace("\n\n", "\n")
                    .replace("\n", " ")
                )
                # If the activity modifier is blank, skip
                if len(mod_name) == 0:
                    continue
                # If the activity modifier is set to only show in nav mode (open Ghost when in the activity), skip
                # Stops what appears to be duplicate modifiers from showing
                if not mod_info["displayInActivitySelection"]:
                    continue

                nfmods += f"  * **{mod_name}**: {mod_description}\n"
                oldmods.append(mod)
        # Get Nightfall adept
        zavala_focused_decoding = api.get_vendor(2232145065)
        if "Featured" in zavala_focused_decoding:
            for item in zavala_focused_decoding["Featured"]:
                item_hash = item["itemHash"]
                item_def = api.item_def(item_hash)
                item_name = item_def["displayProperties"]["name"]
                if "(Adept)" in item_name:
                    nfreward = f"[{item_name}](https://light.gg/db/items/{item_hash})"
                    break

        activities = api.get_activities_full()

        # weekly heroic strike burn
        logger.info("weekly: getting strike burn")
        strike_activity = 743628305
        if strike_activity in activities:
            activity = activities[strike_activity]
            for mod in activity["modifierHashes"]:
                mod_info = api.activity_modifier_def(mod)
                if "Surge" not in mod_info["displayProperties"]["name"]:
                    continue
                heroicstrikeburn = mod_info["displayProperties"]["name"]
                break
        else:
            logger.warning("weekly: no strike milestone?!")

        # weekly featured raid, dungeon, and exotic mission
        # Unfortunately, Bungie uses a new objective hash for each activity, even though the objectives themselves
        # are the same
        # Raid: "Weekly Raid Challenge"
        # Dungeon: Weekly Dungeon Challenge"
        # Exotic: "Weekly Exotic Rotator Challenge"
        logger.info("weekly: getting featured raid, dungeon, and exotic mission")
        for activity_hash in activities:
            if "challenges" in activities[activity_hash]:
                for challenge in activities[activity_hash]["challenges"]:
                    obj_def = api.objective_def(challenge["objective"]["objectiveHash"])
                    if obj_def["displayProperties"]["name"] == "Weekly Raid Challenge":
                        activity_def = api.activity_def(activity_hash)
                        name = activity_def["originalDisplayProperties"]["name"]
                        if len(weekly_raid) == 0 and len(name) > 0:
                            weekly_raid = name
                        elif name not in weekly_raid:  # Avoid dupes from Master versions when they exist.
                            weekly_raid += f", {name}"
                    elif obj_def["displayProperties"]["name"] == "Weekly Dungeon Challenge":
                        activity_def = api.activity_def(activity_hash)
                        name = activity_def["originalDisplayProperties"]["name"]
                        if len(weekly_dungeon) == 0 and len(name) > 0:
                            weekly_dungeon = name
                        elif name not in weekly_dungeon:
                            weekly_dungeon += f", {name}"
                    elif obj_def["displayProperties"]["name"] == "Weekly Exotic Rotator Challenge":
                        activity_def = api.activity_def(activity_hash)
                        name = activity_def["originalDisplayProperties"]["name"]
                        if len(weekly_exotic_mission) == 0 and len(name) > 0:
                            weekly_exotic_mission = name
                        elif name not in weekly_exotic_mission and ("Legend" not in name) and ("Expert" not in name):
                            weekly_exotic_mission += f", {name}"

        logger.info("weekly: getting raid challenges")
        for activity_hash in activities:
            if raids == "*beep boop* failed to fetch":
                raids = ""
            if activity_hash in RAIDS:
                if "modifierHashes" not in activities[activity_hash]:
                    logger.warning(
                        "No challenge modifiers for raid activity %r: %r", activity_hash, activities[activity_hash]
                    )
                    continue
                raids += (
                    format_activity(
                        api=api,
                        activity_hash=activity_hash,
                        h=3,
                        description=False,
                        original=True,
                        link=False,
                        modifiers=activities[activity_hash]["modifierHashes"],
                        exclude=["Boost"],  # Exclude exotic drop rate boosts via triumphs
                    )
                    + "\n"
                )

        # weekly dares dares contestants (on legend)
        logger.info("weekly: getting dares contestants")
        dares_legend_activity_hash = 1030714181
        if dares_legend_activity_hash in activities:
            dares_activity = activities[dares_legend_activity_hash]
            for modifier_hash in dares_activity["modifierHashes"]:
                modifier_def = api.activity_modifier_def(modifier_hash)
                if "Contestant" in modifier_def["displayProperties"]["name"]:
                    weekly_dares_contestants = format_weekly_dares_contestants(modifier=modifier_def)
                    break

        # europa
        logger.info("weekly: getting europa")
        for activity, act_details in activities.items():
            act_info = api.activity_def(activity)
            act_name = act_info["displayProperties"]["name"]
            if not act_name.startswith("Empire Hunt: "):
                continue
            if "modifierHashes" not in act_details:
                continue
            if act_name.endswith(": Adept"):
                act_name = act_name[:-7]
            act_name = act_name[13:]  # Remove Empire Hunt:
            empire_hunt = "[{}](https://destinytracker.com/destiny-2/db/activities/{}): {}".format(
                act_name, activity, act_info["displayProperties"]["description"]
            )
            break

        for activity, act_details in activities.items():
            act_info = api.activity_def(activity)
            act_name = act_info["displayProperties"]["name"]
            if not act_name.startswith("Simulation: "):
                continue
            exo_challenge = "[{}](https://destinytracker.com/destiny-2/db/activities/{}): {}".format(
                act_name, activity, act_info["displayProperties"]["description"]
            )
            break

        # bounties
        logger.info("weekly: getting bounties")
        for vendor, data in WEEKLY_BOUNTY_VENDORS.items():
            try:
                wares = api.get_vendor(vendor)
                if data["index"] not in wares:
                    continue
                if bounties == "":
                    bounties = "---\n#Weekly Bounties\n"
                bounties += "**{}**\n\nName|Description|Cost|Requirement|Reward\n-|-|-|-|-\n".format(data["name"])
                for bounty in wares[data["index"]]:
                    bounties += format_bounty(bounty, api, False, True)
                if vendor == 2917531897:  # Ada-1 research frames
                    for bounty in wares["Research Weapon Frames"]:
                        bounties += format_bounty(bounty, api, False)
                bounties += "\n\n"
            except Exception:  # pylint: disable=broad-except
                if vendor == 919809084:  # No exception needed for Eva
                    logger.info("failed to get vendor %s, hash %i", data["name"], vendor)
                else:
                    logger.exception("failed to get vendor %s, hash %i", data["name"], vendor)
                continue

        # armor
        logger.info("weekly: getting armor")
        armor_sales += "Class|Name|Vendor|Type|MOB|RES|REC|DIS|INT|STR|Total|Cost\n|:-----|:-----|:-----|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:----|:----\n"  # pylint: disable=line-too-long
        for class_name in ("Titan", "Hunter", "Warlock"):
            for vendor, data in WEEKLY_ARMOR_VENDORS.items():
                try:
                    wares = api.get_vendor(vendor, class_name)
                    if data["index"] not in wares:
                        continue
                    for sale in wares[data["index"]]:
                        item = api.item_def(sale["itemHash"])
                        costs = ""
                        for cost in sale["costs"]:
                            cost_item = api.item_def(cost["itemHash"])
                            if costs != "":
                                costs += " & "
                            costs += "{} {}".format(cost["quantity"], cost_item["displayProperties"]["name"])

                        dispname = item["itemTypeDisplayName"]

                        perks = ""
                        if "stats" not in sale["itemComponents"]:
                            continue

                        total = 0
                        single_highest = 0
                        for _, stat in sale["itemComponents"]["stats"].items():
                            if stat["statHash"] not in (
                                144602215,
                                392767087,
                                1735777505,
                                1943323491,
                                2996146975,
                                4244567218,
                            ):
                                continue
                            if perks != "":
                                perks += "|"
                            if int(stat["value"]) < single_limit:
                                perks += str(stat["value"])
                            else:
                                perks += "**" + str(stat["value"]) + "**"
                            total += int(stat["value"])
                            if int(stat["value"]) > single_highest:
                                single_highest = int(stat["value"])

                        if total == 0:
                            continue
                        if total < total_limit and single_highest < single_limit:
                            continue

                        if total >= total_limit:
                            total = "**" + str(total) + "**"
                        armor_sales += "{}|[{}](https://light.gg/db/items/{})|{}|{}|{}|{}|{}\n".format(  # pylint: disable=line-too-long
                            class_name,
                            item["displayProperties"]["name"],
                            sale["itemHash"],
                            data["name"],
                            dispname,
                            perks,
                            total,
                            costs,
                        )
                except Exception:  # pylint: disable=broad-except
                    if vendor == 919809084:  # No exception needed for Eva
                        logger.info("failed to get vendor %s, hash %i", data["name"], vendor)
                    else:
                        logger.exception("failed to get vendor %s, hash %i", data["name"], vendor)
                    continue

        # moon nightmare hunts
        logger.info("weekly: getting nightmare hunts")
        for activity, act_details in activities.items():
            act_info = api.activity_def(activity)
            act_name = act_info["displayProperties"]["name"]
            if not act_name.startswith("Nightmare Hunt: "):
                continue
            if "modifierHashes" not in act_details or len(act_details["modifierHashes"]) > 1:
                continue
            if act_name.endswith(": Adept"):
                act_name = act_name[:-7]
            act_name = act_name[16:]  # remove Nightmare Hunt:
            rotationgnms += " * **Nightmare Hunt**: [{}](https://destinytracker.com/destiny-2/db/activities/{}): {}\n".format(  # pylint: disable=line-too-long
                act_name, activity, act_info["displayProperties"]["description"]
            )

        # Ada-1
        logger.info("weekly: getting Ada-1")
        ada1 = api.get_vendor(350061650)
        for catname, catsales in ada1.items():
            if catname == "Material Exchange":
                for inventory_item in catsales:
                    ada += format_item_with_cost(inventory_item, api)

        # eververse boo
        logger.info("weekly: getting eververse")
        eververse = api.get_vendor(3361454721)
        already = []
        for _, catdata in eververse.items():
            for sale in catdata:
                if "costs" not in sale or len(sale["costs"]) < 1:
                    continue
                if sale["costs"][0]["itemHash"] != 2817410917:  # 2817410917 = Bright Dust
                    continue
                if sale["itemHash"] in already:
                    continue
                already.append(sale["itemHash"])

                item = api.item_def(sale["itemHash"])
                costs = ""
                for cost in sale["costs"]:
                    cost_item = api.item_def(cost["itemHash"])
                    if costs != "":
                        costs += " & "
                    costs += "{} {}".format(cost["quantity"], cost_item["displayProperties"]["name"])
                if ev_bd == "*beep boop* failed to fetch":
                    ev_bd = "Name|Description|Type|Cost\n-|-|-|-\n"
                ev_bd += "[{}](https://light.gg/db/items/{})|{}|{}|{}\n".format(
                    item["displayProperties"]["name"],
                    sale["itemHash"],
                    item["displayProperties"]["description"].replace("\n", " "),
                    item["itemTypeDisplayName"],
                    costs,
                )

        # SEASON TEMP: ROTATIONS
        logger.info("weekly: xxx")
        # SEASON TEMP END

    except Exception:  # pylint: disable=broad-except
        logger.exception("Error creating Weekly Thread")

    return api.process_string_variables(
        TEMPLATE_WEEKLY.format(
            nf=nightfall,
            heroicstrikeburn=heroicstrikeburn,
            weeklydarescontestants=weekly_dares_contestants,
            weeklydaresloot=weekly_dares_loot,
            weeklyraid=weekly_raid,
            weeklydungeon=weekly_dungeon,
            weeklyexoticmission=weekly_exotic_mission,
            raids=raids,
            bounties=bounties,
            cityweek=cityweek,
            citypetra=citypetra,
            citymission=citymission,
            citychallenge=citychallenge,
            citywell=citywell,
            nfmods=nfmods,
            skcampaign=shadowkeep_campaign_mission,
            wanderingnm=wanderingnm,
            rotationgnms=rotationgnms,
            troveguardian=troveguardian,
            nfreward=nfreward,
            evBD=ev_bd,
            eclipsedZone=eclipsed_zone,
            empireHunt=empire_hunt,
            exoChallenge=exo_challenge,
            wqcampaign=witchqueen_campaign_mission,
            wqcampaigndesc=witchqueen_campaign_desc,
            lfcampaign=lightfall_campaign_mission,
            lfcampaigndesc=lightfall_campaign_desc,
            neomunapartitionname=neomuna_parition_name,
            neomunapartitiondesc=neomuna_partition_desc,
            vexincursionzone=neomuna_incursion_zone,
            fscampaign=final_shape_campaign_mission,
            fscampaigndesc=final_shape_campaign_desc,
            armorSales=armor_sales,
            totalLimit=total_limit,
            singleLimit=single_limit,
            weeklyRecords=weekly_records,
            ada=ada,
            # SEASON TEMP
            # SEASON TEMP END
        )
    )


def get_xur(api: BungieAPI, logger: Logger) -> str:
    """Get Xur API data"""
    PLACEHOLDER = ""  # pylint: disable=invalid-name
    XUR_VENDOR_HASH = 2190858386  # pylint: disable=invalid-name
    XUR_MORE_STRANGE_OFFERS_HASH = 537912098  # pylint: disable=invalid-name
    XUR_STRANGE_GEAR_OFFERS_HASH = 3751514131  # pylint: disable=invalid-name

    # default values
    location = "Bazaar, Tower, The Last City"
    currencies_engrams = PLACEHOLDER
    exotic_ciphers = PLACEHOLDER
    exotic_armor = PLACEHOLDER
    exotic_weapons = PLACEHOLDER
    legendary_armor = PLACEHOLDER
    legendary_weapons = PLACEHOLDER
    materials = PLACEHOLDER
    other = PLACEHOLDER

    try:
        logger.info("xur: getting vendor")

        # For most calls, the regular vendor is fine
        xur = api.get_vendor(XUR_VENDOR_HASH)

        # Xur More Strange Offers sub-vendor. Used for material exchange.
        xur_more_strange = api.get_vendor(XUR_MORE_STRANGE_OFFERS_HASH)

        # The "Strange Gear Offers" sub-vendor has different legendary armor per character
        xur_strange_gears = {}
        for character in ("Titan", "Warlock", "Hunter"):
            xur_strange_gears[character] = api.get_vendor(XUR_STRANGE_GEAR_OFFERS_HASH, character)
        xur_strange_gear = xur_strange_gears["Titan"]

        # Xur main vendor
        for category_name, sales in xur.items():
            if category_name == "Multivarious Strange Offers":
                # As of 2024-06-07, the Multivarious Strange Offers can contain currencies, mods, items, or armor
                # Item type 1: Currencies
                # Item type 2: Armor
                # Item type 3: Weapon
                # Item type 8: Engram
                # Item type 20: Dummy
                # Other: just display name and description
                for sale in sales:
                    item = api.item_def(sale["itemHash"])
                    item_type = item["itemType"]
                    if item_type == 1:
                        currencies_engrams += format_item_with_cost(sale, api)
                    elif item_type == 2:
                        exotic_armor += format_armor(sale, api, True)
                    elif item_type == 3:
                        exotic_weapons += format_weapon(sale, api)
                    elif item_type == 8:
                        currencies_engrams += format_item_with_cost(sale, api)
                    else:
                        other += "{}|{}\n".format(
                            item["displayProperties"]["name"],
                            item["displayProperties"]["description"].replace("\n", " "),
                        )

            if category_name == "Exotic Ciphers":
                for sale in sales:
                    item = api.item_def(sale["itemHash"])
                    exotic_ciphers += "{}|{}\n".format(
                        item["displayProperties"]["name"], item["displayProperties"]["description"].replace("\n", " ")
                    )

        # Xur More Strange Offers sub-vendor
        for category_name, sales in xur_more_strange.items():
            if category_name == "Strange Material Offers":
                for sale in sales:
                    item = api.item_def(sale["itemHash"])
                    costs = ""
                    for cost in sale["costs"]:
                        cost_item = api.item_def(cost["itemHash"])
                        if costs != "":
                            costs += " & "
                        costs += "{} {}".format(cost["quantity"], cost_item["displayProperties"]["name"])

                    materials += "* {} ({} for {})\n".format(item["displayProperties"]["name"], sale["quantity"], costs)

        # Xur Strange Gear sub-vendor
        for category_name, sales in xur_strange_gear.items():
            if category_name == "Legendary Weapons":
                for sale in sales:
                    legendary_weapons += format_weapon(sale, api, True)

            if category_name == "Exotic Gear":
                for sale in sales:
                    item = api.item_def(sale["itemHash"])
                    item_type = item["itemType"]
                    if item_type == 3:
                        exotic_weapons += format_weapon(sale, api)
                    else:
                        other += "{}|{}\n".format(
                            item["displayProperties"]["name"],
                            item["displayProperties"]["description"].replace("\n", " "),
                        )

        # This is for the armor only
        for character, xur2 in xur_strange_gears.items():
            for category_name, sales in xur2.items():
                if category_name == "Legendary Armor":
                    for sale in sales:
                        legendary_armor += format_armor(sale, api, True)

    except Exception:  # pylint: disable=broad-except
        logger.exception("Error creating Xur Thread")

    return api.process_string_variables(
        TEMPLATE_XUR.format(
            location=location,
            earmor=exotic_armor,
            lweapons=legendary_weapons,
            larmor=legendary_armor,
            eweapons=exotic_weapons,
            eciphers=exotic_ciphers,
            currenciesengrams=currencies_engrams,
            materials=materials,
            other=other,
        )
    )


def get_trials(api: BungieAPI, logger: Logger):
    """Get Trials API data"""
    # Set template variables to the default before parsing
    tmap = flawless = passages = "*beep boop* failed to fetch"

    try:
        logger.info("trials: getting")

        # Get Trials map from Destiny Trials Report
        dtr_map = format_trials_map()
        if len(dtr_map) != 0:
            tmap = dtr_map
        else:
            tmap = "Surprise!"

        # Get vendor information from Saint-14
        # Returns (when Trials is active):
        #   - Passages
        #   - Rank Rewards
        #   - Bounties (only daily bounties as of Season 15 revamp)
        # As of Lightfall (Season 20), focused decoding is a separate vendor
        # See Saint's vendor definition in the manifest to see sub-vendors
        saint = api.get_vendor(765357505)
        saint_focused_decoding = api.get_vendor(502095006)

        # As of the Season 15 Trials revamp, Saint only offers daily bounties
        if "Passages" in saint:
            passages = ""
            for passage in saint["Passages"]:
                info = api.item_def(passage["itemHash"])
                if not "perks" in info or len(info["perks"]) < 1:
                    continue
                passages += "{}|".format(info["displayProperties"]["name"])

                perk_info = api.sandbox_perk_def(info["perks"][0]["perkHash"])
                passages += "{}|".format(perk_info["displayProperties"]["description"].replace("\n", " "))

                costs = ""
                for cost in passage["costs"]:
                    cost_item = api.item_def(cost["itemHash"])
                    if costs != "":
                        costs += " & "
                    costs += "{} {}".format(cost["quantity"], cost_item["displayProperties"]["name"])
                passages += "{}\n".format(costs)
        else:
            # If there are no passages, assume that Trials is not active
            logger.info("Trials of Osiris is not active")
            return ""

        # Pull out the flawless reward
        # We do this by checking the failure string on a particular reward
        # As of Season 19, the Flawless reward will have the following failure strings:
        # 17: ""
        # See https://data.destinysets.com/i/Vendor:765357505 failureStrings if things change
        if "Weapons" in saint_focused_decoding:
            for item in saint_focused_decoding["Weapons"]:
                item_hash = item["itemHash"]
                item_def = api.item_def(item_hash)
                item_name = item_def["displayProperties"]["name"]
                if "(Adept)" in item_name:
                    flawless = f"[{item_name}](https://light.gg/db/items/{item_hash})"
                    break

    except Exception:  # pylint: disable=broad-except
        logger.exception("Error creating Trials Thread")
        return ""

    return api.process_string_variables(TEMPLATE_TRIALS.format(tmap=tmap, passages=passages, flawless=flawless))


def get_ib(api: BungieAPI, logger: Logger):
    """Get Iron Banner API data"""

    try:
        logger.info("IB: getting crucible")
        active = False
        activities = api.get_activities()
        for activity in activities:
            act = api.activity_def(activity)
            if "Iron Banner" in act["displayProperties"]["name"]:
                active = True
                break

        if not active:
            logger.info("IB is not active.")
            return ""

    except Exception:  # pylint: disable=broad-except
        logger.exception("Error creating IB Thread")
        return ""

    return api.process_string_variables(TEMPLATE_IB)


def get_maintenance(api: BungieAPI, logger: Logger):
    """Get Bungie Global Alert data to determine game maintenance"""
    try:
        alerts = api.get_global_alerts()
        for alert in alerts:
            body = alert["AlertHtml"]
            link = alert["AlertLink"]
            timestamp = datetime.strptime(alert["AlertTimestamp"], "%Y-%m-%dT%H:%M:%S%z")
            if alert["AlertKey"] in ("All-OfflineSoonToday", "D2-OfflineSoonToday"):
                # If the timestamp is not within the day, ignore
                if (datetime.now(tz=timezone.utc) - timestamp).seconds < 86400:
                    return f"[{body}]({link})"

        # Return nothing - no thread created
        logger.info("There is no maintenance scheduled within the day.")
        return ""

    except Exception:  # pylint: disable=broad-except
        logger.exception("Error creating maintenance thread")
        return ""
