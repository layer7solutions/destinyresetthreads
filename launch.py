"""
Main file
Initalizes the bot and starts it
"""

import configparser
import logging
import logging.config
import sys
from datetime import datetime, timezone
from time import sleep
from typing import Callable, Dict

import praw
import prawcore
import urllib3
import yaml
from layer7_utilities import LoggerConfig, oAuth
from requests.exceptions import Timeout

from _version import __version__
from banner import Banner
from bungie_api import BungieAPI
from templates import TEMPLATE_FF
from threadfunctions import get_daily, get_ib, get_maintenance, get_trials, get_weekly, get_xur

config = configparser.ConfigParser()
config.read("botconfig.ini")

__botname__ = "DestinyResetThreads"
__description__ = "Posts Threads with dynamic content at specified dates"
__author__ = "u/Try_to_guess_my_psn"
__dsn__ = config.get("BotConfig", "DSN")
__logpath__ = config.get("BotConfig", "logpath")

_dbusername = config.get("Database", "Username")
_dbpassword = config.get("Database", "Password")
_dbhost = config.get("Database", "Host")

SUBREDDIT = "DestinyTheGame"
OTHERSUBS = [{"name": "LowSodiumDestiny", "flair": "Announcement!"}]


class ResetThreadPoster:
    """Reset thread bot"""

    def __init__(self):
        self.days = {
            "Monday": [],
            "Tuesday": [],
            "Wednesday": [],
            "Thursday": [],
            "Friday": [],
            "Saturday": [],
            "Sunday": [],
            "Daily": [],
        }
        self.dm_handlers = {}
        self.lastchecked = datetime.now(timezone.utc)

        #################### LOGGER SETUP ############################
        # Setup logging
        log_config = LoggerConfig(__dsn__, __botname__, __version__, __logpath__)
        logging.config.dictConfig(log_config.get_config())
        self.logger = logging.getLogger("root")
        self.logger.info("/*********Starting App*********\\")
        self.logger.info("App Name: %s | Version: %s", __botname__, __version__)
        ################################################################

        self.can_launch = self.login()

        # API setup
        self.api = BungieAPI(self.logger, "botconfig.ini")

        # print(get_daily(self.api, self.logger))
        # sys.exit(0)

        if not self.can_launch:
            self.logger.error("login failed :(")
            sys.exit(1)

        # Banner setup
        self.banner = Banner(self.reddit.subreddit(SUBREDDIT), self.logger, "botconfig.ini")

    def login(self):
        """Login to Reddit"""
        try:
            self.logger.debug("/*********Getting Accounts*********\\")
            auth = oAuth()
            auth.get_accounts(
                "dtgbot",
                __description__,
                __version__,
                __author__,
                __botname__,
                _dbusername,
                _dbpassword,
                _dbhost,
                "TheTraveler",
            )
            for account in auth.accounts:
                self.reddit = account.login()
            self.logger.info("Connected to account: %s", self.reddit.user.me())
            return True
        except Exception:  # pylint: disable=broad-except
            self.logger.exception("Failed to log in.")
            return False

    def add_scheduled_thread(
        self,
        day: str,
        hour: int,
        minute: int,
        thread_title: str,
        func: Callable[[BungieAPI, logging.Logger], str],
        dm_keyword: str,
        bottom_sticky: bool = True,
        banner_image: str = "",
    ):
        """Add a scheduled Reddit thread

        day: One of: Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, Daily
        hour: hour in UTC
        minute: minute
        thread_title: Thread title
        func: Function to call that returns the post text
        dm_keyword: keyword that can be sent to the bot trigger this scheduled thread
        bottom_stickY: Do you want this to be the 2nd sticky?
        banner_image: banner image ID
        """
        if not self.can_launch:
            return
        self.days[day].append(
            {
                "title": thread_title,
                "hour": hour,
                "minute": minute,
                "func": func,
                "bottom": bottom_sticky,
                "banner": banner_image,
            }
        )
        self.dm_handlers[dm_keyword] = {
            "title": thread_title,
            "hour": hour,
            "minute": minute,
            "func": func,
            "bottom": bottom_sticky,
            "banner": banner_image,
        }

    def post_thread(self, thread: Dict, now: datetime):
        """Post a thread

        thread: Dictionary of thread information (see add_scheduled_thread for the formatting of this)
        now: datetime
        """
        self.logger.info("Posting thread %s", thread["title"])
        title = "{} [{}]".format(thread["title"], now.strftime("%Y-%m-%d"))
        text = thread["func"](self.api, self.logger)
        if len(text) == 0:
            self.logger.info("Thread was empty - skipping")
            return None
        post = self.reddit.subreddit(SUBREDDIT).submit(title, selftext=text, send_replies=False)
        post.flair.select("8a20bc94-3d45-11e9-9918-0ee7f7befbde")
        post.mod.distinguish()
        if not (now.strftime("%A") == "Friday" and "Daily" in thread["title"]):
            post.mod.sticky(bottom=thread["bottom"])

        # Banner handling
        if now.strftime("%A") == "Sunday" and "Daily" in thread["title"]:
            self.banner.reset()
        if len(thread["banner"]) != 0:
            try:
                if not self.banner.change(thread["banner"]):
                    raise Exception(  # pylint: disable=broad-exception-raised
                        "Banner Change failed, does the picture exist?"
                    )
            except Exception:  # pylint: disable=broad-except
                self.logger.exception("Couldn't change banner!")

        for sub in OTHERSUBS:
            self.logger.debug("Also posting to /r/%s", sub["name"])
            subpost = self.reddit.subreddit(sub["name"]).submit(title, selftext=text, send_replies=False)
            for template in subpost.flair.choices():
                if template["flair_text"] == sub["flair"]:
                    subpost.flair.select(template["flair_template_id"])
                    break

        return post

    def run_poster(self):
        """Run thread poster"""
        now = datetime.now(timezone.utc).replace(second=0)
        today = now.strftime("%A")
        self.logger.debug("Running threads for %s, Time %s", today, now.strftime("%H:%M"))
        for thread in self.days[today] + self.days["Daily"]:
            try:
                # no need to check the day
                posttime = now.replace(hour=thread["hour"], minute=thread["minute"])
                if not (posttime > self.lastchecked and posttime <= now):
                    # date still in the future, not posting
                    continue
                self.post_thread(thread, now)
            except Exception:  # pylint: disable=broad-except
                self.logger.exception("Error posting thread")

        self.lastchecked = now.replace(second=59)

    def check_dms(self):
        """Check private messages sent to the bot"""
        for msg in self.reddit.inbox.unread(limit=50):
            # Only handle messages (DMs)
            if not isinstance(msg, praw.models.reddit.message.Message):
                continue
            if not msg.subject == "ResetThread" and not msg.subject == "FF" and not msg.subject == "Banner":
                continue
            if not msg.author in self.reddit.subreddit(SUBREDDIT).moderator():
                self.logger.warning("[DM] Got DM from non-mod %s", msg.author)
                continue
            msg.mark_read()
            if msg.subject == "FF":
                y_parsed = yaml.safe_load(msg.body)
                if "title" not in y_parsed:
                    msg.reply("Sorry, please provide a title for the FF thread.")
                    continue
                ff_title = y_parsed["title"]
                if "shorttitle" in y_parsed:
                    ff_short = y_parsed["shorttitle"]
                else:
                    ff_short = ff_title
                self.logger.debug("[FF] Got DM, posting thread")
                now = datetime.now(timezone.utc).replace(second=0)
                # 1. Post thread
                post = self.reddit.subreddit(SUBREDDIT).submit(
                    "Focused Feedback: {}".format(ff_title),
                    selftext=TEMPLATE_FF.format(FFshort=ff_short),
                    send_replies=False,
                )
                post.flair.select("8a20bc94-3d45-11e9-9918-0ee7f7befbde")
                post.mod.sticky(bottom=False)
                self.logger.debug("[FF] Thread is %s, updating sidebar", str(post.id))
                # 2. Update sidebar
                start_tag = "[](#FFS)"
                end_tag = "[](#FFE)"
                sidebar_link = "[Focused Feedback {}: {}](http://redd.it/{})".format(
                    now.strftime("%m/%d"), ff_short, post.id
                )
                sidebar = self.reddit.subreddit(SUBREDDIT).wiki["config/sidebar"].content_md
                start = sidebar.index(start_tag)
                end = sidebar.index(end_tag) + len(end_tag)
                self.logger.debug("[FF] Old text - %s\nNew text - %s", sidebar[start:end], sidebar_link)
                if sidebar[start:end] != "{}{}{}".format(start_tag, sidebar_link, end_tag):
                    sidebar = sidebar.replace(sidebar[start:end], "{}{}{}".format(start_tag, sidebar_link, end_tag))
                    self.reddit.subreddit(SUBREDDIT).wiki["config/sidebar"].edit(
                        sidebar, reason="Updating FF Thread Link"
                    )
                else:
                    self.logger.debug("[FF] No update necessary")
                self.logger.debug("[FF] Updated sidebar, updating wiki")
                # 3. Update wiki
                wiki_tag = "[](#AUTOINSERTFF)"
                wiki = self.reddit.subreddit(SUBREDDIT).wiki["focusedfeedback"].content_md
                wiki = wiki.replace("\r\n", "\n")
                wiki = wiki.replace(
                    "{}\n\n".format(wiki_tag),
                    "{}\n\n- [[{}] {}](https://redd.it/{})\n".format(
                        wiki_tag, now.strftime("%Y-%m-%d"), ff_short, post.id
                    ),
                )
                self.reddit.subreddit(SUBREDDIT).wiki["focusedfeedback"].edit(wiki, reason="Adding FF Thread Link")

                self.logger.info("[FF] Updated to %s - Thread %s", ff_title, str(post.id))
                continue
            if msg.subject == "Banner":
                img = msg.body.split(" ")[0]
                try:
                    if not self.banner.change(img, False):
                        raise Exception(  # pylint: disable=broad-exception-raised
                            "Banner Change failed, does the picture exist? If yes, please check the logs!"
                        )
                except Exception as exception:  # pylint: disable=broad-except
                    self.logger.exception("[DM] Couldn't change banner!")
                    msg.reply("Couldn't change banner to {}: {}".format(img, str(exception)))
                    continue
                msg.reply("Banner updated on old/new/mobile reddit :) Thanks for making the sub look nice!")
                continue

            # else --> msg.subject == "ResetThread"
            parts = msg.body.split(" ")
            keyword = parts[0].lower()
            if keyword not in self.dm_handlers:
                self.logger.warning("[DM] Unknown keyword %s", keyword)
                msg.reply(
                    "Sorry, I do not know the thread type {}. Possible thread types are: {}".format(
                        keyword, ", ".join(list(self.dm_handlers.keys()))
                    )
                )
                continue
            thread = self.dm_handlers[keyword]
            if len(parts) > 1:
                # Got an URL, update existing thread
                try:
                    submission = self.reddit.submission(url=parts[1])
                except praw.exceptions.ClientException:
                    self.logger.exception("[DM] Couldn't get details for submission!")
                    msg.reply(
                        "Sorry, you provided an invalid URL ({}). Leave it empty to create a new thread.".format(
                            parts[1]
                        )
                    )  # pylint: disable=line-too-long
                    continue
                if submission.author != self.reddit.user.me():
                    self.logger.warning("[DM] That submission wasn't made by me")
                    msg.reply(
                        "Sorry, I'm not the author of the post {}. Omit the URL to create a new thread.".format(
                            parts[1]
                        )
                    )  # pylint: disable=line-too-long
                    continue
                try:
                    submission.edit(body=thread["func"](self.api, self.logger))
                    msg.reply("Success! I've updated {}".format(parts[1]))
                except Exception:  # pylint: disable=broad-except
                    self.logger.exception("[DM] Error editing thread")
                    msg.reply("An error occured editing the thread. Please contact a dev!")
            else:
                try:
                    # Get the current time and compare to what the time would be for the daily reset.
                    # If before reset, treat the day as the previous day from now.
                    # The maintenance thread only operates on the current day so that must be considered.
                    now = datetime.now(timezone.utc)
                    reset_datetime = datetime.now(timezone.utc).replace(hour=17, minute=0, microsecond=0)
                    if reset_datetime > now and keyword != "maintenance":
                        datetime_to_use = reset_datetime.replace(day=now.day - 1)
                    else:
                        datetime_to_use = now

                    submission = self.post_thread(thread, datetime_to_use)
                    msg.reply("Success! I've posted {}".format(submission.url))
                except Exception:  # pylint: disable=broad-except
                    self.logger.exception("[DM] Error posting thread")
                    msg.reply("An error occured posting the thread. Please contact a dev!")

    def launch(self):
        """Launch the bot"""
        while self.can_launch:
            try:
                self.run_poster()
                self.check_dms()
                self.logger.debug("Sleeping for 60 seconds.")
                sleep(60)

            except UnicodeEncodeError:
                self.logger.exception("Caught UnicodeEncodeError!")

            except prawcore.exceptions.InvalidToken:
                self.logger.warning("API Token Error. Likely on reddits end. Issue self-resolves.")
                sleep(180)
            except prawcore.exceptions.BadJSON:
                self.logger.warning("PRAW didn't get good JSON, probably reddit sending bad data due to site issues.")
                sleep(180)
            except (
                prawcore.exceptions.ResponseException,
                prawcore.exceptions.RequestException,
                prawcore.exceptions.ServerError,
                urllib3.exceptions.TimeoutError,
                Timeout,
            ):
                self.logger.warning("HTTP Requests Error. Likely on reddits end due to site issues.")
                sleep(300)
            except praw.exceptions.APIException:
                self.logger.exception("PRAW/Reddit API Error")
                sleep(30)
            except praw.exceptions.ClientException:
                self.logger.exception("PRAW Client Error")
                sleep(30)
            except KeyboardInterrupt:
                self.logger.info("Caught KeyboardInterrupt - Exiting")
                sys.exit()
            except Exception:  # pylint: disable=broad-except
                self.logger.exception("General Exception - Sleeping 5 min")
                sleep(300)

        self.logger.info("Exiting")
        sys.exit()


def main():
    """Main function"""
    threadpostbot = ResetThreadPoster()
    # addScheduledThread(day, hour, minute, thread_title, func, DMKeyword, bottom_sticky, banner_image)
    # all times UTC (Stupid DST) - " [Y-m-d]" will always be appended at the end of the title
    threadpostbot.add_scheduled_thread("Daily", 13, 10, "Upcoming Maintenance", get_maintenance, "maintenance")
    threadpostbot.add_scheduled_thread("Daily", 17, 4, "[D2] Daily Reset Thread", get_daily, "daily")
    threadpostbot.add_scheduled_thread("Tuesday", 17, 2, "[D2] Weekly Reset Thread", get_weekly, "weekly", False)
    threadpostbot.add_scheduled_thread(
        "Friday", 17, 2, "[D2] Trials of Osiris Megathread", get_trials, "trials", False, "TrialsBanner01"
    )
    threadpostbot.add_scheduled_thread("Friday", 17, 2, "[D2] Xûr Megathread", get_xur, "xur")
    threadpostbot.add_scheduled_thread("Tuesday", 17, 2, "[D2] Iron Banner Megathread", get_ib, "ib", True, "ibanner1")
    threadpostbot.launch()
