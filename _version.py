"""Store version information"""
__version_info__ = (1, 23, "DEV")
__version__ = ".".join(map(str, __version_info__))
