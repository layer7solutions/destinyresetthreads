"""Easy formatting functions for use in thread templates"""

import re
from datetime import datetime, timezone
from typing import Dict, List, Optional, Union

import requests

from bungie_api import BungieAPI


def format_activity(
    *,
    api: BungieAPI,
    activity_hash: int,
    h: int = 0,
    original: bool = False,
    description: bool = True,
    link: bool = True,
    modifiers: Optional[List[int]],
    exclude: Optional[List[str]],
) -> str:
    """Format an activity

    Arguments:
        api -- BungieAPI instance

        activity_hash -- DestinyActivityDefinition hash

        h -- Header level. 1 = H1, 2 = H2, etc. This is the heading level when formatting in Markdown

        original -- Show original activity name?

        description -- Show activity description?

        link -- Link the activity title to an online database?

        modifiers -- A list of DestinyActivityModifierDefinition hashes. If provided, will render these modifiers as
            manifest modifiers may only be active during certain weeks

        exclude -- Set of strings to exclude. If found in a modifier name, the modifier is excluded from the list

    Returns:
        Formatted activity entry
        Example, with h = 1:

        ---

        # Activity Name

        Description here

        * **Modifier 1**: Description of modifier
    """
    activity_def = api.activity_def(activity_hash)
    if original:
        activity_name = activity_def["originalDisplayProperties"]["name"]
        activity_desc = activity_def["originalDisplayProperties"]["description"]
    else:
        activity_name = activity_def["displayProperties"]["name"]
        activity_desc = activity_def["displayProperties"]["description"]
    modifiers_str: str = ""

    # Format modifiers
    if modifiers is None:
        modifiers = []
        for item in activity_def["modifiers"]:
            modifiers.append(item["activityModifierHash"])
    for modifier_hash in modifiers:
        modifier_def = api.activity_modifier_def(modifier_hash)
        modifier_name = modifier_def["displayProperties"]["name"]
        modifier_desc = modifier_def["displayProperties"]["description"].replace("\r\n", "\n").replace("\n", " ")

        # If the modifier name or description is blank, skip it
        if modifier_name == "" or modifier_desc == "":
            continue
        # If the modifier is set to not show in activity selection, skip it
        if not modifier_def["displayInActivitySelection"]:
            continue
        # If this modifier should be excluded, skip it
        # We have a variable here because there is an inner loop where simply breaking or continuing would do nothing
        skip = False
        if exclude is not None:
            for item in exclude:
                if item.lower() in modifier_name.lower():
                    skip = True
                    break
        if skip:
            continue

        modifier_fmt = f"* **{modifier_name}**: {modifier_desc}\n"

        # Add to modifiers str
        modifiers_str += modifier_fmt

    # Build output string
    output: str = ""
    # Start with header
    h_level: str = ""
    if h > 0:
        h_level = ("#" * h) + " "
    if link:
        output += f"{h_level}[{activity_name}](https://destinytracker.com/destiny-2/db/activities/{activity_hash})\n"
    else:
        output += f"{h_level}{activity_name}\n"

    if description:
        output += f"{activity_desc}\n\n"

    # Add modifiers
    output += modifiers_str

    # Make sure there is a final newline
    if output[-1] != "\n":
        output += "\n"
    return output


def format_armor(armor: Dict, api: BungieAPI, include_class: bool = False) -> str:
    """Format an armor roll for output into a table

    armor: a vendor sales entry
    api: BungieAPI wrapper
    include_class: If True, the character class will be prepended to the gear type

    Name | Type | MOB | RES | REC | DIS | INT | STR | Total | Cost
    -|-|-|-|-|-|-|-|-|-
    """

    # Limits. If the armor has a total higher or single stat higher, it will be bolded
    total_limit: int = 59
    single_limit: int = 26

    # Look up the item and determine the class
    item_hash: int = armor["itemHash"]
    item_def: Dict = api.item_def(item_hash)
    item_name: str = item_def["displayProperties"]["name"]
    item_class_type: int = item_def["classType"]

    character_class: str = ""
    if item_class_type == 0:
        character_class = "Titan"
    elif item_class_type == 1:
        character_class = "Hunter"
    elif item_class_type == 2:
        character_class = "Warlock"

    # String representing the costs
    costs: str = ""
    for cost in armor["costs"]:
        cost_item: Dict = api.item_def(cost["itemHash"])
        cost_name: str = cost_item["displayProperties"]["name"]
        cost_quantity: int = cost["quantity"]
        if len(costs) > 0:
            costs += " & "
        costs += f"{cost_quantity} {cost_name}"

    dispname: str = item_def["itemTypeDisplayName"]
    if include_class and character_class not in dispname:
        dispname = f"{character_class} {dispname}"

    perks: str = ""
    # No armor stats?
    if "stats" not in armor["itemComponents"]:
        return ""

    total: int = 0
    for _, stat in armor["itemComponents"]["stats"].items():
        # If the stat is not an armor stat, we are not interested
        if stat["statHash"] not in (
            144602215,  # Intellect
            392767087,  # Resilience
            1735777505,  # Discipline
            1943323491,  # Recovery
            2996146975,  # Mobility
            4244567218,  # Strength
        ):
            continue
        # If already building the stat columns, add divider
        if len(perks) > 0:
            perks += " | "

        # If greater than or equal to threshold, bold
        if int(stat["value"]) >= single_limit:
            perks += "**" + str(stat["value"]) + "**"
        else:
            perks += str(stat["value"])
        total += int(stat["value"])

    # If total is higher than the threshold, bold
    if total >= total_limit:
        total_str = f"**{total}**"
    else:
        total_str = str(total)

    return f"[{item_name}](https://light.gg/db/items/{item_hash}) | {dispname} | {perks} | {total_str} | {costs}\n"


def format_bounty(bounty: Dict, api: BungieAPI, daily: bool = True, force_weekly: bool = False) -> str:
    """
    Format a bounty for output into a table
    bounty: a vendor sales entry
    api: BungieAPI wrapper instance
    daily: Consider daily bounties?
    force_weekly: Only consider bounties with "Weekly" in their name
    NOTE: This function will not render repeatable bounties

    Daily format:
    Name | Description | Requirement | Reward
    -|-|-|-

    Weekly format:
    Name | Description | Cost | Requirement | Reward
    -|-|-|-|-
    """

    info = api.item_def(bounty["itemHash"])
    if "Classified" in info["displayProperties"]["name"]:
        return ""
    if 1784235469 not in info["itemCategoryHashes"]: # <ItemCategory "Bounties">
        return ""
    if daily and "Weekly" in info["itemTypeDisplayName"]:
        return ""
    if force_weekly:
        if "Weekly" not in info["itemTypeDisplayName"]:
            # This is a workaround to allow weekly Cosmodrome bounties to be rendered. Weekly Cosmodrome bounties are of
            # rare type, while the daily bounties are common type.
            if "Rare" not in info["itemTypeAndTierDisplayName"]:
                return ""
    if info["displayProperties"]["name"].lower() == "additional bounties":
        return ""

    out = "{}|{}|".format(
        info["displayProperties"]["name"], info["displayProperties"]["description"].replace("\n", " ")
    )

    if not daily:
        costs = ""
        for cost in bounty["costs"]:
            cost_item = api.item_def(cost["itemHash"])
            if costs != "":
                costs += " & "
            costs += "{} {}".format(cost["quantity"], cost_item["displayProperties"]["name"])
        out += "{}|".format(costs)

    objectives = ""
    if "objectives" in info:
        for obj_hash in info["objectives"]["objectiveHashes"]:
            obj = api.objective_def(obj_hash)
            if objectives != "":
                objectives += " & "
            objectives += "{} {}".format(obj["completionValue"], obj["progressDescription"])
        out += "{}|".format(objectives)
    else:
        out += "|"

    rewards = ""
    for reward in info["value"]["itemValue"]:
        if reward["itemHash"] == 0:
            continue
        # Nicer Display of "zero" rewards (= Engram or sth without number before it)
        if reward["quantity"] == 0:
            reward["quantity"] = ""
        reward_info = api.item_def(reward["itemHash"])
        # This is a workaround to stop weekly Cosmodrome bounties from appearing in daily threads if the bounty has a
        # reward of 3582080006 (XP++), treat it as weekly. As of 2022-03-05, only weekly bounties award XP++ or higher.
        if daily and reward_info["hash"] == 3582080006:
            return ""
        if rewards != "":
            rewards += " & "
        rewards += "{} {}".format(reward["quantity"], reward_info["displayProperties"]["name"])
    out += rewards

    out += "\n"

    return out


def format_challenges(challenges: Dict) -> str:
    """Format challenges in Name - *Text* format"""
    out = ""
    if len(challenges) == 0:
        out += "*beep boop* failed to fetch"
    for challenge in challenges:
        if challenge[1] == "":
            continue
        if out != "":
            out += "|"
        out += "{} - *{}*".format(challenge[0], challenge[1])
    out += "\n"

    return out


def format_item_with_cost(sale: Dict, api: BungieAPI) -> str:
    """Format an item with a cost for output into a table

    sale: a vendor sales entry
    api: BungieAPI wrapper instance

    Name | Description | Cost
    -|-|-
    """
    # Look up the item represented in the sale
    item_def: Dict = api.item_def(sale["itemHash"])

    # Easy alises for use later
    name: str = item_def["displayProperties"]["name"]
    description: str = item_def["displayProperties"]["description"].replace("\n\n", "\n").replace("\n", " ")
    costs: str = ""
    for cost in sale["costs"]:
        cost_item: Dict = api.item_def(cost["itemHash"])
        cost_name: str = cost_item["displayProperties"]["name"]
        cost_quantity: int = cost["quantity"]
        if len(costs) > 0:
            costs += " & "
        costs += f"{cost_quantity} {cost_name}"

    return f"{name} | {description} | {costs}\n"


def format_lost_sectors(api: BungieAPI, activity_ids: List[int]) -> str:
    """Format lost sectors for output into a table

    api: BungieAPI instance
    hash: list of DestinyActivityDefinition hashes for the lost sector difficulties,
    sorted from lowest to highest difficulty
    """

    output = ""
    for index, activity_id in enumerate(activity_ids):
        activity = api.activity_def(activity_id)
        output += "### [{}](https://destinytracker.com/destiny-2/db/activities/{})\n".format(
            activity["displayProperties"]["name"], activity["hash"]
        )

        # For now, we are parsing the description for modifiers because the Champion/Shielded Foes will be incorrect
        # in the manifest, despite the activity being specific to the difficulty. Oh well.
        for line in activity["displayProperties"]["description"].replace("\n\n", "\n").split("\n"):
            # Example:
            # Legend Difficulty: Locked Equipment, Match Game, Extra Shields

            # First step, add ** ** around the "Prefix:" so that it shows bold in the first
            prefix = re.search("^.+:", line)
            line = line.replace(prefix.group(), "**" + prefix.group() + "**")

            # Second step, add it to a list
            output += "* " + line + "\n"

        # Add a newline to add a space between entries
        if index != len(activity_ids) - 1:
            output += "\n"

    return output


def format_trials_map() -> str:
    """
    Get the current Trials map from DestinyTrialsReport

    If the current map is not available, an empty string will be returned instead

    Example return value: [Map title](image_url)
    This string can contain multiple maps if there are multiple maps in rotation, comma+space separated
    """
    # Fetch API data from Destiny Trials Report
    dtr_response: requests.Response = requests.get("https://api.trialsofthenine.com/weeks/0", timeout=10)
    if dtr_response.status_code != 200:
        return ""

    # Parse JSON response
    dtr: dict = dtr_response.json()

    # Extract the end date as a workable Python datetime
    # Example: 2022-06-25 17:00:00
    end_date = datetime.strptime(dtr["endDate"], "%Y-%m-%d %H:%M:%S").replace(tzinfo=timezone.utc)

    # If the end date is less than the current time, DTR has not updated their API yet
    if datetime.now(timezone.utc) > end_date:
        return ""

    # The result string, to be returned
    result: str = ""

    # Build the response string
    # It is rare, but sometimes Trials can have a rotation of maps in a weekend. This logic handles all cases, whether
    # there is one map or multiple
    for i, item in enumerate(dtr["maps"]):
        map_name: str = item["name"]
        image_url: str = "https://www.bungie.net" + item["imagePath"]
        format_str: str = f"[{map_name}]({image_url})"

        if i < len(dtr["maps"]) - 1:
            result += format_str + ", "
        else:
            result += format_str

    return result


def format_vendor_location(api: BungieAPI, vendor_id: int) -> str:
    """Format a vendor location

    Output is in the format of "Destination Name, Place"
    Example: "The Last City, Earth"
    """
    # Get the vendor's destination hash
    destination_hash: int = api.get_vendor_location(vendor_id)

    # Look up the destination and place
    destination_def: Dict = api.destination_def(destination_hash)
    place_def: Dict = api.place_def(destination_def["placeHash"])

    # Easy alises
    destination_name: str = destination_def["displayProperties"]["name"]
    place_name: str = place_def["displayProperties"]["name"]
    return f"{destination_name}, {place_name}"


def format_weapon(weapon: Dict, api: BungieAPI, masterwork: bool = False) -> str:
    """Format a weapon for use in a table

    weapon: a sale from a vendor category
    masterwork: include the masterwork information in the table?
    """
    # Look up the item
    item: Dict = api.item_def(weapon["itemHash"])

    # Item type display name
    # Ex. "Primary Hand Cannon"
    # Kinetic, Energy, Heavy
    item_bucket: str = ""
    if item["inventory"]["bucketTypeHash"] == 1498876634:  # Kinetic Weapons
        item_bucket = "Kinetic "
    elif item["inventory"]["bucketTypeHash"] == 2465295065:  # Energy Weapons
        item_bucket = "Energy "
    elif item["inventory"]["bucketTypeHash"] == 953998645:  # Power Weapons
        item_bucket = "Heavy "
    display_name = item["itemTypeDisplayName"]

    item_name: str = item["displayProperties"]["name"]
    item_hash: int = item["hash"]
    item_type_display: str = f"{item_bucket}{display_name}"

    # The result is built through a series of routines, starting with the name and type of weapon
    result: str = f"[{item_name}](https://light.gg/db/items/{item_hash}) | {item_type_display}"

    # No rolled perks/traits? No problem.
    if (
        "reusablePlugs" not in weapon["itemComponents"]
        or "plugs" not in weapon["itemComponents"]["reusablePlugs"]
        or len(weapon["itemComponents"]["reusablePlugs"]["plugs"]) == 0
    ):
        return f"{result}\n"

    # Build up every column of perks
    for _, plugs in weapon["itemComponents"]["reusablePlugs"]["plugs"].items():
        # Current recorded "perks" for this column. Always starts blank.
        currentplugs: str = ""

        for plug in plugs:
            # Empty or otherwise uselss plug
            if "plugItemHash" not in plug:
                continue

            # Look up the item represented by the plug
            pluginfo: Dict = api.item_def(plug["plugItemHash"])

            # Easy alieses for use later
            pluginfo_name: str = pluginfo["displayProperties"]["name"]
            pluginfo_description: str = pluginfo["displayProperties"]["description"]

            # We are not interested in shaders/ornaments/masterworks etc
            if "Shader" in pluginfo_name:
                continue
            if "Masterwork" in pluginfo_description and not masterwork:
                continue

            # If a "column" has more than one perk already added, add a divider
            if len(currentplugs) > 0:
                currentplugs += " // "

            # Add it
            currentplugs += pluginfo_name

        # If this processing of a "column" contained something useful, write it to the output
        if len(currentplugs) > 0:
            result += f" | {currentplugs}"

    return f"{result}\n"


def format_weekly_dares_contestants(*, modifier: dict) -> str:
    """
    Format the weekly Dares of Eternity contestants when doing Legend difficulty

    modifier: DestinyActivityModifierDefinition
    """
    name: str = modifier["displayProperties"]["name"]
    description_list: list = modifier["displayProperties"]["description"].split("\n")
    contestants: str = ""
    for i, contestant_round in enumerate(description_list):
        if i != (len(description_list) - 1):
            contestants += f"* {contestant_round}\n"
        else:
            contestants += f"* {contestant_round}"

    return f"### {name}\n{contestants}"


def format_weekly_dares_loot(*, api: BungieAPI, loot: Dict[str, Union[int, str]]) -> str:
    """Format the Dares of Eternity loot table

    Arguments:
        api -- BungieAPI instance
        loot -- the loot table, expceted format: {"weapons": [int], "armor": [str]}

    Returns:
        Bulleted list
        * **Armor**: "suit one", "suit two"
        * **Weapons**: Weapon 1, Weapon 2

        (The weapons will be linked to Light.gg)
    """
    armor: str = ""
    weapons: str = ""

    for i, armor_set in enumerate(loot["armor"]):
        if i != (len(loot["armor"]) - 1):
            armor += armor_set + ", "
        else:
            armor += armor_set

    for i, item_hash in enumerate(loot["weapons"]):
        item = api.item_def(item_hash)
        name = item["displayProperties"]["name"]
        formatted = f"[{name}](https://light.gg/db/items/{item_hash})"

        if i != (len(loot["weapons"]) - 1):
            weapons += formatted + ", "
        else:
            weapons += formatted

    return f"* **Armor**: {armor}\n* **Weapons**: {weapons}"
