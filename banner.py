"""Utilities for changing the subreddit banner"""
import os
import re
import tempfile
import urllib.request
from configparser import ConfigParser
from logging import Logger

import psycopg2
from rediscluster import RedisCluster


class Banner:
    """Utility for changing a subreddit banner"""

    def __init__(self, sub, logger: Logger, path_to_cfg: str):
        self.logger = logger
        self.sub = sub

        cfg_file = ConfigParser()
        cfg_file.read(path_to_cfg)

        self._dbusername = cfg_file.get("Database", "Username")
        self._dbpassword = cfg_file.get("Database", "Password")
        self._dbhost = cfg_file.get("Database", "Host")

        # Create the redis connection
        redis_hosts = cfg_file.get("Redis", "HOST").split(",")
        redis_password = cfg_file.get("Redis", "PASSWORD")
        node_list = []
        for node in redis_hosts:
            temp_d = {"host": node, "port": "6379"}
            node_list.append(temp_d)

        self.redis = RedisCluster(startup_nodes=node_list, decode_responses=True, password=redis_password)

        self.logger.info("Banner connected to DB")

    def change(self, image, auto: bool = True) -> bool:
        """Change all banner images to an image

        auto: Should we store this image as the last automatically-changed banner?
        """
        self.logger.info("Changing all banner images to %s", image)

        i = self._get_image(image)

        if not i:
            return False

        if auto:
            self.redis.set("dtg.drt.banner_lastauto", image)
            if not self.redis.exists("dtg.drt.banner_restore"):
                self.redis.set("dtg.drt.banner_restore", i["current"])

        self._change_old(image)
        self._change_new(i["newfile"])
        self._change_mobile(i["newfile"])

        os.unlink(i["newfile"])

        return True

    def reset(self) -> bool:
        """Reset/restore banner, if possible"""
        if not self.redis.exists("dtg.drt.banner_restore"):
            return False

        i = self._get_image(self.redis.get("dtg.drt.banner_restore"), False)

        # Banner we want to restore doesn't exist anymore
        if not i:
            return False

        # Banner was changed, but not by us
        if i["current"] != self.redis.get("dtg.drt.banner_lastauto"):
            return True

        self.change(self.redis.get("dtg.drt.banner_restore"), False)
        self.redis.delete("dtg.drt.banner_restore")

        return True

    def _get_image(self, image, download=True):
        self.logger.info("Getting old & new banner image")

        style = self.sub.stylesheet()

        pattern = re.compile(r"#header-bottom-left:before{background:url\(%%([\w-]+)%%\)")
        cur_image = pattern.search(style.stylesheet).group(1)
        self.logger.debug("Current image is %s", cur_image)

        url = ""
        for i in style.images:
            if i["name"] == image:
                url = i["url"]
                break
        if len(url) == 0:
            self.logger.warn("Couldn't find image uploaded to old reddit")
            return False

        if not download:
            return {"current": cur_image}

        self.logger.debug("Downloading old reddit banner image %s", url)
        file_name = tempfile.mkstemp(suffix=".png")[1]
        with urllib.request.urlopen(url) as response, open(file_name, "wb") as out_file:
            data = response.read()
            out_file.write(data)

        self.logger.info("Current image is %s, new image is saved in %s", cur_image, file_name)

        return {"current": cur_image, "newfile": file_name}

    def _change_old(self, image):
        self.logger.info("Changing old banner image to %s", image)

        style = self.sub.stylesheet()
        css = style.stylesheet

        pattern = re.compile(r"#header-bottom-left:before{background:url\(%%([\w-]+)%%\)")
        css = pattern.sub("#header-bottom-left:before{{background:url(%%{}%%)".format(image), css)
        self.sub.stylesheet.update(css, "ResetThread update banner")

        self.logger.info("Changing on destinyreddit web")
        pattern = re.compile(r"%%([\w-]+)%%")

        conn = psycopg2.connect(
            host=self._dbhost, dbname="application", user=self._dbusername, password=self._dbpassword
        )
        conn.autocommit = True
        cursor = conn.cursor()
        cursor.execute("SELECT styles FROM dtg_css_modules WHERE mod_name=%s", ["Custom Banner (Day Mode)"])
        daystyle = cursor.fetchone()[0]
        cursor.execute("SELECT styles FROM dtg_css_modules WHERE mod_name=%s", ["Custom Banner (Night Mode)"])
        nightstyle = cursor.fetchone()[0]
        daystyle = pattern.sub("%%{}%%".format(image), daystyle)
        nightstyle = pattern.sub("%%{}%%".format(image), nightstyle)
        cursor.execute("UPDATE dtg_css_modules SET styles=%s WHERE mod_name=%s", [daystyle, "Custom Banner (Day Mode)"])
        cursor.execute(
            "UPDATE dtg_css_modules SET styles=%s WHERE mod_name=%s", [nightstyle, "Custom Banner (Night Mode)"]
        )

    def _change_new(self, image_file):
        self.logger.info("Changing new banner image to %s", image_file)

        self.logger.debug("Uploading new reddit banner image %s", image_file)
        self.sub.stylesheet.upload_banner(image_file)

    def _change_mobile(self, image_file):
        self.logger.info("Changing mobile banner image to %s", image_file)

        self.logger.debug("Uploading mobile reddit banner image %s", image_file)
        image_type = "mobileBannerImage"
        image_url = self.sub.stylesheet._upload_style_asset(image_path=image_file, image_type=image_type)
        self.sub.stylesheet._update_structured_styles({image_type: image_url})
