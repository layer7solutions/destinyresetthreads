"""Bungie.net API Wrapper"""
import re
from collections import OrderedDict
from configparser import ConfigParser
from datetime import datetime, timedelta
from enum import Enum
from logging import Logger
from typing import Dict, List

import requests
from cachetools.func import ttl_cache
from rediscluster import RedisCluster


class BungieMembershipType(Enum):
    """Bungie Membership Type

    https://bungie-net.github.io/multi/schema_BungieMembershipType.html#schema_BungieMembershipType
    """

    NONE = 0
    TIGER_XBOX = 1  # Xbox
    TIGER_PSN = 2  # PlayStation
    TIGER_STEAM = 3  # Steam
    TIGER_BLIZZARD = 4  # Battle.net
    TIGER_STADIA = 5  # Google Stadia
    TIGER_EGS = 6  # Epic Game Store
    TIGER_DEMON = 10
    BUNGIE_NEXT = 254
    ALL = -1


class DestinyCharacterClass(Enum):
    """Destiny character class

    This enum is purely for internal use. Not related to Bungie API enums.
    """

    TITAN = "Titan"
    WARLOCK = "Warlock"
    HUNTER = "Hunter"


class BungieAccessToken:
    """Retrieves a Bungie.net access token and stores it in Redis, refreshing as needed"""

    def __init__(self, logger: Logger, path_to_cfg):
        self.logger = logger
        self.path_to_cfg = path_to_cfg
        self.cfg_file = ConfigParser()
        self.cfg_file.read(self.path_to_cfg)
        # Create the redis connection
        redis_hosts = self.cfg_file.get("Redis", "HOST").split(",")
        redis_password = self.cfg_file.get("Redis", "PASSWORD")
        node_list = []
        for node in redis_hosts:
            temp_d = {"host": node, "port": "6379"}
            node_list.append(temp_d)

        self.redis = RedisCluster(startup_nodes=node_list, decode_responses=True, password=redis_password)

        self.oauth_token_url = "https://www.bungie.net/Platform/App/OAuth/token/"
        self.client_id = self.cfg_file.get("app", "clientid")
        self.client_secret = self.cfg_file.get("app", "clientsecret")
        if self.redis.exists("dtg.drt.bngapi_refreshtoken"):
            self.refresh_token = self.redis.get("dtg.drt.bngapi_refreshtoken")
        else:
            self.refresh_token = self.cfg_file.get("app", "refreshtoken")
        if self.redis.exists("dtg.drt.bngapi_accesstoken"):
            self.access_token = self.redis.get("dtg.drt.bngapi_accesstoken")
            self.access_token_expiry = datetime.now() + timedelta(seconds=self.redis.ttl("dtg.drt.bngapi_accesstoken"))
        else:
            self.access_token = "INVALID"
            self.access_token_expiry = datetime.now()

    @property
    def token(self):
        """Retrieve access token"""
        if self.access_token_expiry > datetime.now():
            return self.access_token

        self.logger.info("access token expired, getting new")
        request = requests.post(
            self.oauth_token_url,
            data={
                "grant_type": "refresh_token",
                "client_id": self.client_id,
                "client_secret": self.client_secret,
                "refresh_token": self.refresh_token,
                "response_type": "code",
            },
            timeout=60,
        )
        request.raise_for_status()

        json = request.json()
        self.access_token = json["access_token"]
        self.access_token_expiry = datetime.now() + timedelta(seconds=int(json["expires_in"]))
        self.refresh_token = json["refresh_token"]

        # save the new refreshtoken in redis
        self.redis.set("dtg.drt.bngapi_accesstoken", self.access_token, ex=json["expires_in"])
        self.redis.set("dtg.drt.bngapi_refreshtoken", self.refresh_token)

        return self.access_token


class BungieAPI:
    """Create an API instance"""

    def __init__(self, logger: Logger, path_to_cfg):
        self.logger = logger

        cfg_file = ConfigParser()
        cfg_file.read(path_to_cfg)
        self.api_key = cfg_file.get("app", "apikey")

        self.access_token = BungieAccessToken(logger, path_to_cfg)

        self.base_url = "https://www.bungie.net/Platform/Destiny2/"
        self.manifest = {}
        self.manifest_version = ""
        self.manifest_timeout = datetime.now()
        self.manifest_timeout_threshold = timedelta(days=2)

        # Current mod used for API data: Techman
        self.member_id = "4611686018506320669"
        self.member_type = BungieMembershipType.TIGER_STEAM.value
        self.characters = {
            DestinyCharacterClass.HUNTER.value: "2305843009843964123",
            DestinyCharacterClass.TITAN.value: "2305843009685874119",
            DestinyCharacterClass.WARLOCK.value: "2305843009843964122",
        }

    @property
    def headers(self) -> Dict:
        """Returns HTTP headers needed to make API requests"""
        return {"X-API-Key": self.api_key, "Authorization": "Bearer {}".format(self.access_token.token)}

    def get_all_milestones(self, character=DestinyCharacterClass.TITAN.value):
        """Get all milestones"""
        self.logger.debug("API: called get_all_milestones")
        request = requests.get(
            "{}/{}/Profile/{}/Character/{}/?components=202".format(
                self.base_url, self.member_type, self.member_id, self.characters[character]
            ),
            headers=self.headers,
            timeout=60,
        )
        request.raise_for_status()
        return request.json()

    def get_public_milestones(self):
        """Get public milestones"""
        self.logger.debug("API: called get_public_milestones")
        request = requests.get("{}/Milestones/".format(self.base_url), headers=self.headers, timeout=60)
        request.raise_for_status()

        return request.json()["Response"]

    # https://github.com/DestinyItemManager/DIM/blob/master/src/app/progress/progress.tsx
    def get_milestones(self, character=DestinyCharacterClass.TITAN.value):
        """Get milestones"""
        self.logger.debug("API: called get_milestones")
        json = self.get_all_milestones(character)

        return json["Response"]["progressions"]["data"]["milestones"]

    def get_vendor(self, vendor_id: int, character=DestinyCharacterClass.TITAN.value):
        """Get vendor information"""
        self.logger.debug("API: called get_vendor")
        request = requests.get(
            "{}/{}/Profile/{}/Character/{}/Vendors/{}/?components=400,401,402,300,301,302,303,304,305,306,307,308,309,310".format(  # pylint: disable=line-too-long
                self.base_url, self.member_type, self.member_id, self.characters[character], vendor_id
            ),
            headers=self.headers,
            timeout=60,
        )
        request.raise_for_status()
        json = request.json()["Response"]
        sales = json["sales"]["data"]
        item_comp = json["itemComponents"]

        # map item numbers to cat indexes
        cat_map = {}
        i = 0
        for cat in json["categories"]["data"]["categories"]:
            for index in cat["itemIndexes"]:
                cat_map[index] = cat["displayCategoryIndex"]
            i += 1

        # get categories from the vendor
        vendor = self.vendor_def(vendor_id)
        cat_sales = {}
        i = 0
        for cat in vendor["displayCategories"]:
            cat_sales[i] = {"name": cat["displayProperties"]["name"], "items": []}
            i += 1

        # assign items on sale
        for index, sale in sales.items():
            index = int(index)
            if index not in cat_map:
                self.logger.warning("item not found in catMap")
                continue
            cat = cat_map[index]
            if cat not in cat_sales:
                self.logger.warning("itemcat {} not found in catlist".format(cat))
                cat = 0
                if cat not in cat_sales:
                    cat_sales[cat] = {"name": "", "items": []}
            # Append itemComponents info to sale (item)
            sale["itemComponents"] = {}
            str_index = str(index)
            avail_comps = [
                "instances",
                "perks",
                "renderData",
                "stats",
                "plugObjectives",
                "sockets",
                "talentGrids",
                "plugStates",
                "objectives",
                "reusablePlugs",
            ]
            for comp in avail_comps:
                if comp in item_comp and str_index in item_comp[comp]["data"]:
                    if comp in item_comp[comp]["data"][str_index]:
                        sale["itemComponents"][comp] = item_comp[comp]["data"][str_index][comp]
                    else:
                        sale["itemComponents"][comp] = item_comp[comp]["data"][str_index]
            cat_sales[cat]["items"].append(sale)

        # remove empty entries
        return_sales = OrderedDict()
        for i, sales in cat_sales.items():
            if len(sales["items"]) > 0:
                return_sales.update({sales["name"]: sorted(sales["items"], key=lambda l: l["vendorItemIndex"])})

        return return_sales

    def get_vendor_location(self, vendor_id: int, character=DestinyCharacterClass.TITAN.value):
        """Get location of a vendor

        This is probably most useful for Xur. Returns a DestinyDestinationDefinition hash.
        """
        self.logger.debug("API: called getVendorLocation")
        request = requests.get(
            "{}/{}/Profile/{}/Character/{}/Vendors/{}/?components=400".format(
                self.base_url, self.member_type, self.member_id, self.characters[character], vendor_id
            ),
            headers=self.headers,
            timeout=60,
        )
        request.raise_for_status()
        location_index = request.json()["Response"]["vendor"]["data"]["vendorLocationIndex"]
        vendor = self.vendor_def(vendor_id)
        return vendor["locations"][location_index]["destinationHash"]

    def get_activities(self, character=DestinyCharacterClass.TITAN.value) -> List[int]:
        """Returns a list of activity hashes"""
        self.logger.debug("API: called get_activities")
        request = requests.get(
            "{}/{}/Profile/{}/Character/{}/?components=204".format(
                self.base_url, self.member_type, self.member_id, self.characters[character]
            ),
            headers=self.headers,
            timeout=60,
        )
        request.raise_for_status()
        resp = sorted(
            request.json()["Response"]["activities"]["data"]["availableActivities"],
            key=lambda act: act["displayLevel"] if "displayLevel" in act else 0,
        )

        activities = []
        for activity in resp:
            activities.append(activity["activityHash"])

        return activities

    def get_activities_full(self, character=DestinyCharacterClass.TITAN.value) -> Dict:
        """Return full activity information"""
        self.logger.debug("API: called get_activities_full")
        request = requests.get(
            "{}/{}/Profile/{}/Character/{}/?components=204".format(
                self.base_url, self.member_type, self.member_id, self.characters[character]
            ),
            headers=self.headers,
            timeout=60,
        )
        request.raise_for_status()
        resp = sorted(
            request.json()["Response"]["activities"]["data"]["availableActivities"],
            key=lambda act: act["displayLevel"] if "displayLevel" in act else 0,
        )

        activities = {}
        for activity in resp:
            activities[activity["activityHash"]] = activity

        return activities

    def get_activities_from_interactables(self, character=DestinyCharacterClass.TITAN.value) -> List[int]:
        """Get list of activities from activity interactables
        Interactable activities are ones that exist in the game world only, e.g. campaign flags. This can be
        cross-referenced with availableActivities for those that cannot be launched from the Director.
        """
        self.logger.debug("API: called get_activities_from_interactables")
        request = requests.get(
            "{}/{}/Profile/{}/Character/{}/?components=204".format(
                self.base_url, self.member_type, self.member_id, self.characters[character]
            ),
            headers=self.headers,
            timeout=60,
        )
        request.raise_for_status()
        resp = sorted(
            request.json()["Response"]["activities"]["data"]["availableActivityInteractables"],
            key=lambda act: act["activityInteractableElementIndex"] if "activityInteractableElementIndex" in act else 0,
        )

        activities = []
        for item in resp:
            interactable_def = self.activity_interactable_def(item["activityInteractableHash"])
            for entry in interactable_def["entries"]:
                activities.append(entry["activityHash"])

        return activities

    def get_activities_with_objective(self, objective_hash: int, character=DestinyCharacterClass.TITAN.value):
        """Get list of activities with a given objective.
        This is not the activity definition itself. This is because modifiers need to be fetched in the API, as the
        manifest will contain all possible modifiers.

        Example response:

        {'activityHash': 743628305, 'isNew': False, 'canLead': True, 'canJoin': True, 'isCompleted': False,
        'isVisible': True, 'displayLevel': 50, 'recommendedLight': 1350, 'difficultyTier': 2, 'modifierHashes':
        [3362074814, 4293586104, 4221013735], 'booleanActivityOptions': {'494695110': True}}
        """
        self.logger.debug("API: called get_activities_with_objectives")
        request = requests.get(
            "{}/{}/Profile/{}/Character/{}/?components=204".format(
                self.base_url, self.member_type, self.member_id, self.characters[character]
            ),
            headers=self.headers,
            timeout=60,
        )
        request.raise_for_status()
        resp = sorted(
            request.json()["Response"]["activities"]["data"]["availableActivities"],
            key=lambda act: act["displayLevel"] if "displayLevel" in act else 0,
        )

        activities = []
        for item in resp:
            activity = self.activity_def(item["activityHash"])
            matches = False
            if "challenges" not in activity:
                continue
            for challenge in activity["challenges"]:
                if "objectiveHash" not in challenge:
                    continue
                if challenge["objectiveHash"] == objective_hash:
                    matches = True
                    break
            if matches:
                activities.append(item)

        return activities

    def get_activities_with_modifier(self, modifier_hash: int, character=DestinyCharacterClass.TITAN.value) -> List:
        """Get a list of activities with given modifer"""
        self.logger.debug("API: called getActivitiesWithObjective")
        request = requests.get(
            "{}/{}/Profile/{}/Character/{}/?components=204".format(
                self.base_url, self.member_type, self.member_id, self.characters[character]
            ),
            headers=self.headers,
            timeout=60,
        )
        request.raise_for_status()
        resp = sorted(
            request.json()["Response"]["activities"]["data"]["availableActivities"],
            key=lambda act: act["displayLevel"] if "displayLevel" in act else 0,
        )

        activities = []
        for activity in resp:
            if "modifierHashes" not in activity:
                continue
            if modifier_hash in activity["modifierHashes"]:
                activities.append(activity)

        return activities

    def get_challenges(self, activity_id) -> List:
        """Get a list of challenges"""
        # web parser, not yet available via API
        # see https://github.com/Bungie-net/api/issues/432
        self.logger.debug("API: called getChallenges")
        request = requests.get(
            "https://www.bungie.net/en/Explore/Detail/DestinyActivityDefinition/{}".format(activity_id), timeout=60
        )
        request.raise_for_status()

        return re.findall(
            r'<div class="text-content">\s*<div class="title">([^<]*)</div>\s*<div class="subtitle">([^<]*)</div>\s*</div>',  # pylint: disable=line-too-long
            request.text,
        )

    def get_global_alerts(self) -> List:
        """Get Bungie Global Alerts"""
        # https://bungie-net.github.io/multi/schema_GlobalAlert.html#schema_GlobalAlert
        self.logger.debug("API: called get_global_alerts")
        request = requests.get("https://www.bungie.net/Platform/GlobalAlerts/", timeout=60)
        request.raise_for_status()
        return request.json()["Response"]

    def process_string_variables(self, text: str):
        """Process rich Destiny text (string variables)"""
        pattern = re.compile(r"({var:(\d+)})", re.IGNORECASE | re.MULTILINE)
        matches = pattern.findall(text)
        if matches:
            # Fetch the map of variables to values
            string_vars = self.get_string_variables()
            # If the map is empty (probably due to an error), just send the text back
            if len(string_vars) == 0:
                return text

            # If we do have a map, replace the values
            result = text
            for capture_groups in matches:
                # 0: {var:xxx}
                # 1: xxx
                result = result.replace(capture_groups[0], str(string_vars[capture_groups[1]]))
            return result
        return text

    @ttl_cache(ttl=timedelta(hours=1), timer=datetime.now)
    def get_string_variables(self, character=DestinyCharacterClass.TITAN.value) -> Dict[str, int]:
        """Get the map of Destiny StringVariables

        If there is an error in the API, an empty dictionary will be returned
        """
        self.logger.debug("API: called get_string_variables")
        try:
            r = requests.get(
                "{}/{}/Profile/{}/?components=1200".format(self.base_url, self.member_type, self.member_id),
                headers=self.headers,
                timeout=60,
            )
            r.raise_for_status()
            res = r.json()["Response"]["characterStringVariables"]["data"][self.characters[character]][
                "integerValuesByHash"
            ]
            return res
        except requests.HTTPError:
            self.logger.exception("Request failed: %s", r.text)
            return {}

    # :TODO: that's always the same...
    def milestone_def(self, milestone_id: int) -> Dict:
        """Get DestinyMilestoneDefinition"""
        json = self.manifest_get("DestinyMilestoneDefinition")

        if str(milestone_id) not in json:
            return None

        return json[str(milestone_id)]

    def activity_def(self, activity_id: int):
        """Get DestinyActivityDefinition"""
        json = self.manifest_get("DestinyActivityDefinition")

        if str(activity_id) not in json:
            return None

        return json[str(activity_id)]

    def activity_interactable_def(self, activity_interactable_id: int):
        """Get DestinyActivityInteractableDefinition"""
        json = self.manifest_get("DestinyActivityInteractableDefinition")

        if str(activity_interactable_id) not in json:
            return None

        return json[str(activity_interactable_id)]

    def destination_def(self, destination_id: int):
        """Get DestinyDestinationDefinition"""
        json = self.manifest_get("DestinyDestinationDefinition")
        if str(destination_id) not in json:
            return None

        return json[str(destination_id)]

    def place_def(self, place_id: int):
        """Get DestinyPlaceDefinition"""
        json = self.manifest_get("DestinyPlaceDefinition")
        if str(place_id) not in json:
            return None

        return json[str(place_id)]

    def place_name(self, place_id: int) -> str:
        """Get name from DestinyPlaceDefinition"""
        json = self.manifest_get("DestinyPlaceDefinition")

        if str(place_id) not in json:
            return None

        return json[str(place_id)]["displayProperties"]["name"]

    def item_def(self, item_id: int):
        """Get DestinyInventoryItemDefinition"""
        json = self.manifest_get("DestinyInventoryItemDefinition")

        if str(item_id) not in json:
            return None

        return json[str(item_id)]

    def objective_def(self, objective_id):
        """Get DestinyObjectiveDefinition"""
        json = self.manifest_get("DestinyObjectiveDefinition")

        if str(objective_id) not in json:
            return None

        return json[str(objective_id)]

    def vendor_def(self, vendor_id: int):
        """Get DestinyVendorDefinition"""
        json = self.manifest_get("DestinyVendorDefinition")

        if str(vendor_id) not in json:
            return None

        return json[str(vendor_id)]

    def activity_modifier_def(self, mod_id: int):
        """Get DestinyActivityModifierDefinition"""
        json = self.manifest_get("DestinyActivityModifierDefinition")

        if str(mod_id) not in json:
            return None

        return json[str(mod_id)]

    def sandbox_perk_def(self, perk_id: int):
        """Get DestinySandboxPerkDefinition"""
        json = self.manifest_get("DestinySandboxPerkDefinition")

        if str(perk_id) not in json:
            return None

        return json[str(perk_id)]

    def inventory_bucket_def(self, bucket_id: int):
        """Get DestinyInventoryBucketDefinition"""
        json = self.manifest_get("DestinyInventoryBucketDefinition")

        if str(bucket_id) not in json:
            return None

        return json[str(bucket_id)]

    def presentation_node_def(self, presentation_node_id: int):
        """Get DestinyPresentationNodeDefinition"""
        json = self.manifest_get("DestinyPresentationNodeDefinition")

        if str(presentation_node_id) not in json:
            return None

        return json[str(presentation_node_id)]

    def record_def(self, record_id: int):
        """Get DestinyRecordDefinition"""
        json = self.manifest_get("DestinyRecordDefinition")

        if str(record_id) not in json:
            return None

        return json[str(record_id)]

    def socket_type_def(self, socket_type_id: int):
        """Get DestinySocketTypeDefinition"""
        json = self.manifest_get("DestinySocketTypeDefinition")

        if str(socket_type_id) not in json:
            return None

        return json[str(socket_type_id)]

    def plug_set_def(self, plugset_id: int):
        """Get DestinyPlugSetDefinition"""
        json = self.manifest_get("DestinyPlugSetDefinition")

        if str(plugset_id) not in json:
            return None

        return json[str(plugset_id)]

    def get_manifest(self):
        """Get Bungie API manifest"""
        if len(self.manifest) > 0 and self.manifest_timeout > datetime.now():
            return self.manifest

        self.logger.debug("API: called get_manifest")
        request = requests.get("{}/Manifest/".format(self.base_url), headers=self.headers, timeout=120)
        request.raise_for_status()
        json = request.json()["Response"]
        if json["version"] == self.manifest_version:
            return self.manifest

        manifest_url = json["jsonWorldContentPaths"]["en"]
        self.logger.info("Getting new manifest %r", json["version"])
        manifest = requests.get("https://www.bungie.net/{}".format(manifest_url), headers=self.headers, timeout=120)
        manifest.raise_for_status()
        self.manifest = manifest.json()
        self.manifest_version = json["version"]
        self.manifest_timeout = datetime.now() + self.manifest_timeout_threshold

        return self.manifest

    def manifest_get(self, table):
        """Get table from manifest"""
        self.logger.debug("API: called ManifestGet")
        manifest = self.get_manifest()

        if table not in manifest:
            raise Exception(f"Table {table} not in manifest")  # pylint: disable=broad-exception-raised

        return manifest[table]
