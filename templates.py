"""Reddit thread templates"""

TEMPLATE_WEEKLY = """# Nightfall - The Ordeal: {nf}

### Modifiers:
{nfmods}

### Rewards:
- **Grandmaster reward**: {nfreward}
- **Pinnacle reward**: Earn 200,000 cumulative points by completing Nightfalls.

---

# Vanguard Surge: {heroicstrikeburn}

The other modifiers rotate daily, check out the Daily Reset Thread for them!

---

# Weekly Rotators
## Featured PvE Activities

* Raid: **{weeklyraid}**
* Dungeon: **{weeklydungeon}**
* Exotic Mission: **{weeklyexoticmission}**

## Raid Challenges & Modifiers
{raids}

---

# Dares of Eternity: Legend
{weeklydarescontestants}

### Loot
{weeklydaresloot}

---

# Pale Heart of the Traveler
## Cooperative Focus Mission: {fscampaign}
{fscampaigndesc}

---

# Legacy Activities

* **Neomuna**
  * **Campaign Mission**: {lfcampaign}: {lfcampaigndesc}
  * **{neomunapartitionname}**: {neomunapartitiondesc}
  * **Vex Incursion Zone**: {vexincursionzone}

* **Savathûn's Throne World**
  * **Campaign Mission**: {wqcampaign}: {wqcampaigndesc}

* **Europa**
  * **Eclipsed Zone**: {eclipsedZone}
  * **Empire Hunt**: {empireHunt}
  * **Exo Challenge**: {exoChallenge}

* **Moon**:
  * **Campaign Mission**: {skcampaign}
  * **Wandering Nightmare**: {wanderingnm}
  * **Trove Guardian** is in *{troveguardian}*

* **Dreaming City**: {cityweek} Curse
  * **Petra** is at {citypetra}.
  * **Weekly Mission**: {citymission}
  * **Ascendant Challenge**: {citychallenge}
  * **Blind Well**: {citywell}

---

# Miscellaneous
### Ada-1's Wares

Name | Description | Cost
-|-|-
{ada}

---

# Eververse Bright Dust Offerings

{evBD}

{bounties}

---

# Seasonal Challenges

Name|Description|Objectives|Rewards
-|-|-|-
{weeklyRecords}

---

# Notable Armor Rolls

*Only notable rolls (stat total >= {totalLimit} or single stat >= {singleLimit}) are listed here.*

{armorSales}

---

[Archie](https://i.imgur.com/ai5QQ3f.png) wishes you a happy reset and good luck!

---

^Never ^forget ^what ^was ^lost. ^While ^the ^API ^[protests](https://en.wikipedia.org/wiki/2023_Reddit_API_controversy) ^have ^concluded, ^Reddit ^remains ^[hostile](https://en.wikipedia.org/wiki/Enshittification#Reddit) ^to ^its ^users.

"""

TEMPLATE_DAILY = """# Daily Modifiers

{vanguardops}

{dares}

{onslaught}

{activitymodifs}

---

# Seasonal

## Expert/Master Lost Sector

{ls}

---

# Pale Heart of the Traveler
## Overthrow: {overthrow}
{overthrowdesc}

---

# Misc
* **Terminal Overload: {terminallocation} Weapon**: {terminalweapon}
* **The Wellspring: {wellspringmode} Weapon**: {wellspringweapon}
* **Altars of Sorrow Weapon**: {altarweapon}

---

# Guns & Materials

### Banshee's Featured Weapons

Name|Type|Column 1|Column 2|Column 3|Column 4|Masterwork
-|-|-|-|-|-|-
{gunsmith}


*Note: Fixed perks on weapons are not displayed*

### Master Rahool's Material Exchange

{rahoolmats}

---

# Bounties

{bounties}

---

[Archie](https://i.imgur.com/ai5QQ3f.png) wishes you a happy reset and good luck!

---

^Never ^forget ^what ^was ^lost. ^While ^the ^API ^[protests](https://en.wikipedia.org/wiki/2023_Reddit_API_controversy) ^have ^concluded, ^Reddit ^remains ^[hostile](https://en.wikipedia.org/wiki/Enshittification#Reddit) ^to ^its ^users.

"""

TEMPLATE_XUR = """# Xûr, Agent of the Nine

A peddler of strange curios, Xûr's motives are not his own. He bows to his distant masters, the Nine.

## Location

***{location}***

---

*SGA: Check [Xûr's](https://www.light.gg/db/vendors/2190858386) [three](https://www.light.gg/db/vendors/3751514131) [inventories](https://www.light.gg/db/vendors/537912098) online to ensure he is offering items at the lowest prices!*

## Exotic Weapons
Name | Type | Column 1 | Column 2 | Column 3 | Column 4
-|-|-|-|-|-
{eweapons}

*Note: Fixed perks on weapons are not displayed*

## Exotic Armor
Name | Type | MOB | RES | REC | DIS | INT | STR | Total | Cost
-|-|-|-|-|-|-|-|-|-|
{earmor}

*Note: Only fixed rolls are displayed here*

## Exotic Ciphers
Name | Description
-|-
{eciphers}

## Currencies & Engrams
Name | Description | Cost
-|-|-
{currenciesengrams}

## Other
Name | Description
-|-
{other}

## Materials
{materials}

## Legendary Weapons
Name | Type | Column 1 | Column 2 | Column 3 | Column 4|Masterwork
-|-|-|-|-|-|-
{lweapons}

*Note: Fixed perks on weapons are not displayed*

## Legendary Armor
Name | Type | MOB | RES | REC | DIS | INT | STR | Total | Cost
-|-|-|-|-|-|-|-|-|-|
{larmor}

---

**What's a Xûr?**

Xûr, Agent of the Nine, is a strange vendor who appears weekly in the Tower. Xûr sells Exotic equipment and only takes Strange Coins in exchange for them.

**TL;DR:** He's the Santa Claus of Destiny and every weekend is Christmas. Sometimes he brings you what you want, sometimes he brings you coal. Mostly it's coal.

**When does Xûr visit?**

Xûr visits every Friday at 17:00 UTC and departs at Weekly Reset (Tuesday 17:00 UTC). If you would like to see all the live conversions of Time Zones, please follow [this link here.](https://www.timeanddate.com/worldclock/timezone/utc)

***Sort comments by New to join the conversation!***

---

[Archie](https://i.imgur.com/ai5QQ3f.png) wishes you a happy reset and good luck!

---

^Never ^forget ^what ^was ^lost. ^While ^the ^API ^[protests](https://en.wikipedia.org/wiki/2023_Reddit_API_controversy) ^have ^concluded, ^Reddit ^remains ^[hostile](https://en.wikipedia.org/wiki/Enshittification#Reddit) ^to ^its ^users.

"""

TEMPLATE_TRIALS = """# Trials of Osiris is LIVE

This thread is for all general discussion, questions, thoughts, musings, wonderings, etc. for the Trials of Osiris.

---

## FAQ

### What are the Trials of Osiris?

 * Trials of Osiris is the pinnacle PvP activity. Every Weekend, the best players compete in 3v3 Elimination for one goal: Go Flawless.

 * To start, head to Saint-14 in the Tower Hangar and buy one of the possible passages (see below).

 * To reach Flawless and get to the Lighthouse, you need to win 7 matches without losing one.

 * It uses connection and weekly performance based matchmaking, which means you'll face teams with a similar amount of wins on their card. Matches will get harder as you win more matches on that same card, and for all cards, the matches you get will be based on your overall performance so far for that week.

 * There is fireteam matchmaking. However, we still recommend you find a team for yourself!

 * Power Level matters, however bonus power from the artifact is not enabled.

### How Long do the Trials of Osiris last?

 * Event Starts: Every Friday at Daily Reset (1700 UTC).

 * Event Ends: Following Tuesday at Weekly Reset (1700 UTC).

### Where do I go to find Guardians to team up with?

 * You can use the in-game Fireteam Finder or head over to /r/Fireteams, www.The100.io, Xbox LFG system, DestinyLFG.net or DestinyLFG.com, or go to the Bungie.net recruitment forum (also available through the Bungie App). Additionally, many Discord servers host fireteam LFG services.

### What if I have a question about another piece of armor/weapon or general Trials question?

 * Use Control + F (Or Command + F if on a Mac) and search for keywords in your question. Someone may have asked it already. If not, ask below in the comments.

---

## Trials of Osiris Map

***{tmap}***

---

## Rewards

* Reputation Rank 4: Upgrade Module (2)

* Reputation Rank 7: Enhancement Prism (3)

* Reputation Rank 10: Trials Weapon _(Changes for each rank reset)_

* Reputation Rank 13: Upgrade Module (2)

* Reputation Rank 16: Trials Weapon _(Changes for each rank reset)_

* Flawless Reward: **{flawless}**

---

## Reputation System, Trials Engrams, and Adept Farming

Win individual rounds within each match to gain Trials reputation. The amount of reputation you gain increases with each round you've won on your card. Earn enough reputation, and you'll be able to claim a **Trials Engram** from Saint-14! This engram can be focused into any currently available Trials loot you have previously obtained, or it can be redeemed for a random Trials drop. **Your reputation increases after every match completion**, based on the number of round-wins on your card, regardless of the result of that match itself (win or lose, 0-5 or 5-4).

Once you have gone Flawless, keep playing! Every win you achieve while at the 7-win level, even if you lose your Flawless, has a chance to drop bonus Trials Engrams, adept weapons, prisms, and even Ascendant Shards. There is no penalty for losing once you've made it to the Lighthouse!

When you're done, you can cash in your 7-win passage for one additional adept drop, granted you have gone Flawless that week. This resets your card so you can start anew.

---

## Passages

Name|Perk|Cost
-|-|-
{passages}

---

***Sort comments by New to join the conversation!***

---

[Archie](https://i.imgur.com/ai5QQ3f.png) wishes you a happy reset and good luck!

---

^Never ^forget ^what ^was ^lost. ^While ^the ^API ^[protests](https://en.wikipedia.org/wiki/2023_Reddit_API_controversy) ^have ^concluded, ^Reddit ^remains ^[hostile](https://en.wikipedia.org/wiki/Enshittification#Reddit) ^to ^its ^users.

"""

TEMPLATE_IB = """# Iron Banner is LIVE

> "Let the Iron Banner shape you."

-Saladin Forge

---

# FAQ

## What is the Iron Banner?

 * Iron Banner is a 2-week-long Crucible event in Destiny 2 that offers its participants unique rewards. More info can be found in the [official Destiny 2: Iron Banner Guide](https://help.bungie.net/hc/en-us/articles/360048722012).
 * Iron Banner offers unique versions of other Crucible game modes, such as Control and Rift. The current game type can be found by viewing the activity details in the director or looking at the modifiers.
 * Iron Banner uses [outlier protection](https://www.bungie.net/7/en/News/article/this-week-in-destiny-02-22-24) matchmaking.

## How Long does Iron Banner last?

 * Event begins: Tuesday at Weekly Reset (1700 UTC).
 * Event ends: The Tuesday 2 weeks later at Weekly Reset (1700 UTC).

## Where do I go to find Guardians to game within the Iron Banner?

 * You can use the in-game Fireteam Finder or head over to /r/Fireteams, www.The100.io, Xbox LFG system, DestinyLFG.net or DestinyLFG.com, or go to the Bungie.net recruitment forum (also available through the Bungie App).

## What if I have a question about another piece of armor/weapon or general Iron Banner question?

 * Use Control + F (Or Command + F if on a Mac) and search for keywords in your question. Someone may have asked it already. If not, ask below in the comments.

## How to earn Iron Banner Rewards

* Complete Iron Banner matches to earn Iron Banner Reputation. This reputation awards Iron Engrams and unlocks various rewards along the Rank Rewards track.
    * Earn bonus reputation by equipping Iron Banner gear or ornaments, an Iron Banner Emblem, and completing Daily Challenges.
    * Iron Engrams can be focused at Lord Saladin into a weapon or armor piece. Items must already be unlocked in Collections to be available for focusing.
* Complete Daily Challenges to earn Pinnacle Gear and boost your Reputation multiplier. These challenges are visible when hovering over the Iron Banner playlist in the Director.
* You have a chance to get randomly rolled Iron Banner gear at the end of matches.

---

[Archie](https://i.imgur.com/ai5QQ3f.png) wishes you a happy reset and good luck!

---

^Never ^forget ^what ^was ^lost. ^While ^the ^API ^[protests](https://en.wikipedia.org/wiki/2023_Reddit_API_controversy) ^have ^concluded, ^Reddit ^remains ^[hostile](https://en.wikipedia.org/wiki/Enshittification#Reddit) ^to ^its ^users.

"""  # pylint: disable=line-too-long

TEMPLATE_FF = """Hello Guardians,

Focused Feedback is where we take the week to focus on a 'Hot Topic' discussed extensively around the Tower.

We do this in order to consolidate Feedback, to get out all your ideas and issues surrounding the topic in one place for discussion and a source of feedback to the Vanguard.

This Thread will be active until next week when a new topic is chosen for discussion

**Whilst Focused Feedback is active, ALL posts regarding '{FFshort}' following its posting will be removed and re-directed to this thread. Exceptions to this rule are as follows: New information / developments, Guides and general questions**

Any and all Feedback on the topic is welcome.

Regular Sub rules apply so please try to keep the conversation on the topic of the thread and keep it civil between contrasting ideas

A Wiki page - [Focused Feedback](https://www.reddit.com/r/DestinyTheGame/wiki/focusedfeedback/) - has also been created for the Sub as an archive for these topics going forward so they can be looked at by whoever may be interested or just a way to look through previous hot topics of the sub as time goes on.

---

[Archie](https://i.imgur.com/ai5QQ3f.png) wishes you a happy reset and good luck!

---

^Never ^forget ^what ^was ^lost. ^While ^the ^API ^[protests](https://en.wikipedia.org/wiki/2023_Reddit_API_controversy) ^have ^concluded, ^Reddit ^remains ^[hostile](https://en.wikipedia.org/wiki/Enshittification#Reddit) ^to ^its ^users.

"""  # pylint: disable=line-too-long
